//
//  SVGUtils.h
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 11/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SVGUtils : NSObject
+ (CGRect)SVGStringToRect:(NSString*)serializedRect;
+ (CGRect)makeDrawingRect:(CGRect)boxRect bound:(CGRect)bound;
+ (NSString *)documentPath;
@end
