//
//  DhtNotificationView.m
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtNotificationView.h"
#import "Utils.h"

@interface DhtNotificationView() {
    bool _isSwipeDismissing;
}
@end
@implementation DhtNotificationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self != nil) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSize().width,  64)];
        _containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _containerView.opaque = false;
        [self addSubview:_containerView];
        _containerView.exclusiveTouch = true;
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
        [_containerView addGestureRecognizer:panRecognizer];
    }
    return self;
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *result = [_contentView hitTest:point withEvent:event];
    return result;
}

- (void)setContentView:(UIView *)view {
    [_contentView removeFromSuperview];
    _contentView = view;
    view.frame = _containerView.bounds;
    [_containerView addSubview:view];
}

- (void)animateIn {
    _isDismissed = false;
    UIWindow *window = _weakWindow;
    CGRect frame = _containerView.frame;
    frame.origin.x = 0;
    _containerView.frame = frame;
    frame.origin = CGPointZero;
    if (window.hidden || !CGRectEqualToRect(_containerView.frame, frame)) {
        window.hidden = false;
        CGRect startFrame = frame;
        startFrame.origin.y = -frame.size.height;
        _containerView.frame = startFrame;
        
        [UIView animateWithDuration:0.3 animations:^{
             _containerView.frame = frame;
         }];
    }
}

- (void)animateOut {
    _isDismissed = true;
    UIWindow *window = _weakWindow;
    if (window.hidden) {
        return;
    }

    CGRect frame = _containerView.frame;
    frame.origin.y = -frame.size.height;
    [UIView animateWithDuration:0.3 animations:^{
         _containerView.frame = frame;
    } completion:^(__unused BOOL finished) {
         window.hidden = true;
    }];
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (!_isDismissed) {
            CGRect frame = _containerView.frame;
            frame.origin.x = [recognizer translationInView:self].x;
            _containerView.frame = frame;
        }
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        float velocity = [recognizer velocityInView:self].x;
        if (ABS(velocity) < 120.0f) {
            CGRect frame = _containerView.frame;
            frame.origin.x = 0;
            [UIView animateWithDuration:0.3 animations:^{
                 _containerView.frame = frame;
            }];
        } else {
            if (ABS(velocity) < 300) {
                velocity = velocity < 0 ? -300 : 300;
            }
            
            CGRect frame = _containerView.frame;
            if (velocity < 0) {
                frame.origin.x = -frame.size.width;
            } else {
                frame.origin.x = frame.size.width;
            }
            NSTimeInterval duration = ABS(frame.origin.x - _containerView.frame.origin.x) / ABS(velocity);
            _isSwipeDismissing = true;
            [UIView animateWithDuration:duration animations:^{
                 _containerView.frame = frame;
            } completion:^(BOOL finished) {
                 if (finished) {
                     _isSwipeDismissing = false;
                     UIWindow *window = _weakWindow;
                     window.hidden = true;
                 }
            }];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_isSwipeDismissing) {
        CGRect frame = _containerView.frame;
        frame.origin.x = 0;
        [UIView animateWithDuration:0.3 animations:^{
             _containerView.frame = frame;
        }];
    }
    [super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_isSwipeDismissing) {
        CGRect frame = _containerView.frame;
        frame.origin.x = 0;
        [UIView animateWithDuration:0.3 animations:^{
             _containerView.frame = frame;
        }];
    }
    [super touchesCancelled:touches withEvent:event];
}
@end
