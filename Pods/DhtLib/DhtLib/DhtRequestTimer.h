//
//  DhtRequestTimer.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 3/7/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DhtRequestTimer : NSObject
@property (nonatomic) NSTimeInterval timeoutDate;

- (id)initWithTimeout:(NSTimeInterval)timeout repeat:(bool)repeat completion:(dispatch_block_t)completion queue:(dispatch_queue_t)queue;
- (void)start;
- (void)fireAndInvalidate;
- (void)invalidate;
- (bool)isScheduled;
- (void)resetTimeout:(NSTimeInterval)timeout;
- (NSTimeInterval)remainingTime;
@end
