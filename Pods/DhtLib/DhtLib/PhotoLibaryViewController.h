//
//  PhotoLibaryViewController.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//


#import "ImagePickerController.h"
#import "DhtHandle.h"

@interface PhotoLibaryViewController : UIViewController
{
    bool _hatTargetNavigationItem;
}

@property (nonatomic, weak) id<ImagePickerControllerDelegate> delegate;
@property (nonatomic, strong) DhtHandle *handler;
@property (nonatomic) bool autoActivateSearch;
@property (nonatomic) bool hideSearchControls;
@property (nonatomic) bool avatarSelectionMode;
- (id)initWithAvatarSelection:(bool)avatarSelection;
@end
