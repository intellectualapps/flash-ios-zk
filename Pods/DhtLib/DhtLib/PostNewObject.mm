//
//  PostNewObject.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/6/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "PostNewObject.h"
#import "ImagePickerAsset.h"
#import "Utils.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation PostNewObject
- (instancetype) init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void) commonInit {
    _image = nil;
    _assetUrl = @"";
    _imageName = @"";
    _desc = @"";
}

- (UIImage *) imageFromALAsset:(ALAsset *)asset {
    if (asset) {
        UIImageOrientation orientation = UIImageOrientationUp;
        NSNumber* orientationValue = [asset valueForProperty:@"ALAssetPropertyOrientation"];
        if (orientationValue != nil) {
            orientation =  (UIImageOrientation)[orientationValue intValue];
        }
        UIImage *img = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage] scale:1.0 orientation:orientation];
        return img;
    }
    return nil;
}

- (instancetype) initWithAsset:(ImagePickerAsset *)asset {
    self = [super init];
    if (self) {
        [self commonInit];
        _image = [Utils cropImageCorrespondingWithCurrentDevice:[self imageFromALAsset:asset.asset]];
        _imageName = [[asset.asset defaultRepresentation] filename];
        _assetUrl = asset.assetUrl;
        _createDate = [NSDate date];
    }
    return self;
}
@end
