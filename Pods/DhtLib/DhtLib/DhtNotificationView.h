//
//  DhtNotificationView.h
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DhtNotificationView : UIView

@property (nonatomic, weak) UIWindow        *weakWindow;
@property (nonatomic) bool      isDismissed;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *contentView;

- (void)animateIn;
- (void)animateOut;
@end
