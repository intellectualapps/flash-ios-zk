//
//  DhtActionControl.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtActionControl.h"
#import "DhtRequestTimer.h"
#include <vector>
#import <libkern/OSAtomic.h>
#import "KLogger.h"

static const char *mainGraphQueueSpecific = "Dht.com.actionqueue.name";
static const char *globalGraphQueueSpecific = "Dht.com.actionqueue.name-global";
static const char *highGraphQueueSpecific = "Dht.com.actionqueue.name-hight";
static const char *graphQueueSpecific = "Dht.com.graphdispatchqueue";
static dispatch_queue_t mainGraphQueue = nil;
static dispatch_queue_t globalGraphQueue = nil;
static dispatch_queue_t highPriorityGraphQueue = nil;

static volatile OSSpinLock removeWatcherRequestsLock = OS_SPINLOCK_INIT;
static volatile OSSpinLock removeWatcherFromPathRequestsLock = OS_SPINLOCK_INIT;

DhtActionControl *actionControlInstance(){
    static DhtActionControl *singleton = nil;
    static dispatch_once_t onceToken;
    /*This will make this class thread safe access*/
    dispatch_once(&onceToken, ^
                  {
                      singleton = [[DhtActionControl alloc] init];
                  });
    
    return singleton;
}

@interface DhtActionControl() {
    std::vector<std::pair<DhtHandle *, NSString *> > _removeWatcherFromPathRequests;
    std::vector<DhtHandle *> _removeWatcherRequests;
}
@property (nonatomic, strong) NSMutableDictionary *liveNodeWatchers;
@property (nonatomic, strong) NSMutableDictionary *requestQueues;
@property (nonatomic, strong) NSMutableDictionary *activeRequests;
@property (nonatomic, strong) NSMutableDictionary *actorMessagesWatchers;
@property (nonatomic, strong) NSMutableDictionary *cancelRequestTimers;
@end

@implementation DhtActionControl
/*
 Concurrent dispatch queue 
 - is use full when you have multiple tasks that can run in parallel.
 - is still a queue in that dequeues task in a first-in, first-out order. 
 Howevery concurrent queue may dequeue other tasks before any previous task finish
 dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
 */
- (bool)isCurrentQueueActionQueue {
    if (dispatch_get_specific(graphQueueSpecific) != NULL) {
        return true;
    } else {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        if (queue == [self globalStageDispatchQueue] || queue == highPriorityGraphQueue)
            return true;
    }
    return false;
}

- (void)dispatchOnHighPriorityQueue:(dispatch_block_t)block {
    if ([self isCurrentQueueActionQueue]) {
        block();
    } else {
        if (highPriorityGraphQueue == NULL) {
            [self globalStageDispatchQueue];
        }
        
        if (highPriorityGraphQueue) {
            dispatch_async(highPriorityGraphQueue, block);
        }
    }
}

- (dispatch_queue_t)globalStageDispatchQueue {
    if (mainGraphQueue == NULL) {
        mainGraphQueue = dispatch_queue_create(mainGraphQueueSpecific, 0);
        
        globalGraphQueue = dispatch_queue_create(globalGraphQueueSpecific, 0);
        dispatch_set_target_queue(globalGraphQueue, mainGraphQueue);
        
        highPriorityGraphQueue = dispatch_queue_create(highGraphQueueSpecific, 0);
        dispatch_set_target_queue(highPriorityGraphQueue, mainGraphQueue);
        
        if (&dispatch_queue_set_specific != NULL) {
            dispatch_queue_set_specific(mainGraphQueue, graphQueueSpecific, (void *)graphQueueSpecific, NULL);
            dispatch_queue_set_specific(globalGraphQueue, graphQueueSpecific, (void *)graphQueueSpecific, NULL);
            dispatch_queue_set_specific(highPriorityGraphQueue, graphQueueSpecific, (void *)graphQueueSpecific, NULL);
        }
    }
    return globalGraphQueue;
}

- (void) dispatchOnActionQueue:(dispatch_block_t)block {
    bool isGraphQueue = false;
    if (dispatch_get_specific(mainGraphQueueSpecific) != NULL) {
        isGraphQueue = true;
    } else {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        isGraphQueue = (queue == [self globalStageDispatchQueue] || queue == highPriorityGraphQueue);
    }
    
    if (isGraphQueue) {
        block();
    } else {
        dispatch_async([self globalStageDispatchQueue], block);
    }
}

- (id) init {
    self = [super init];
    if (self) {
        _activeRequests = [[NSMutableDictionary alloc]init];
        _requestQueues  = [[NSMutableDictionary alloc]init];
        _liveNodeWatchers  = [[NSMutableDictionary alloc]init];
        _actorMessagesWatchers= [[NSMutableDictionary alloc]init];
        _cancelRequestTimers = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void) actionCompleted:(NSString *)action result:(id)result error:(NSError *)error
{
    [self dispatchOnActionQueue:^{
        NSMutableDictionary *requestInfo = [_activeRequests objectForKey:action];
        if (requestInfo != nil) {
            DhtActor *requestBuilder = [requestInfo objectForKey:@"requestBuilder"];
            NSMutableArray *actionWatchers = [requestInfo objectForKey:@"watchers"];
            [_activeRequests removeObjectForKey:action];
            
            for (DhtHandle *handle in actionWatchers) {
                id<DhtWatcher> watcher = handle.delegate;
                if (watcher != nil) {
                    if ([watcher respondsToSelector:@selector(actorProcessCompletedAtpath:result:error:)]) {
                        [watcher actorProcessCompletedAtpath:action result:result error:error];
                    }
                    
                    if (handle.willReleaseOnMainThread)
                        dispatch_async(dispatch_get_main_queue(), ^ { [watcher class]; });
                    watcher = nil;
                }
            }
            
            [actionWatchers removeAllObjects];
            
            if (requestBuilder == nil) {
                [KLogger log:(@"***** Warning ***** requestBuilder is nil")];
            } else if (requestBuilder.requestQueueName != nil) {
                [self removeRequestFromQueueAndProceedIfFirst:requestBuilder.requestQueueName fromRequestBuilder:requestBuilder];
            }
        }
    }];
}

- (void)removeRequestFromQueueAndProceedIfFirst:(NSString *)name fromRequestBuilder:(DhtActor *)requestBuilder {
    NSMutableArray *requestQueue = [_requestQueues objectForKey:requestBuilder.requestQueueName == nil ? name : requestBuilder.requestQueueName];
    if (requestQueue != nil || requestQueue.count ==0) {
        if ([requestQueue objectAtIndex:0] == requestBuilder) {
            [requestQueue removeObjectAtIndex:0];
            if (requestQueue.count>0) {
                DhtActor *nextRequest =nil;
                id nextRequestOptions = nil;
                nextRequest = [requestQueue objectAtIndex:0];
                nextRequestOptions = nextRequest.storedOptions;
                nextRequest.storedOptions = nil;
                if (nextRequest != nil && !nextRequest.cancelled) {
                    [nextRequest execute:nextRequestOptions];
                }
            } else {
                [_requestQueues removeObjectForKey:requestBuilder.requestQueueName];
            }
            
        } else {
            if ([requestQueue containsObject:requestBuilder]) {
                [requestQueue removeObject:requestBuilder];
            }
        }
    }
}

/*
 Get generic path from path. Will use this value to compare with each generic path of each actor.
 */
- (NSString *)genericPathForParametrizedPath:(NSString *)path {
    if (path == nil)
        return @"";
    
    int length = (int)path.length;
    unichar newPath[path.length];
    int newLength = 0;
    
    SEL sel = @selector(characterAtIndex:);
    unichar (*characterAtIndexImp)(id, SEL, NSUInteger) = (typeof(characterAtIndexImp))[path methodForSelector:sel];
    
    bool skipCharacters = false;
    bool skippedCharacters = false;
    
    for (int i = 0; i < length; i++) {
        unichar c = characterAtIndexImp(path, sel, i);
        if (c == '(') {
            skipCharacters = true;
            skippedCharacters = true;
            newPath[newLength++] = '@';
        } else if (c == ')') {
            skipCharacters = false;
        } else if (!skipCharacters) {
            newPath[newLength++] = c;
        }
    }
    
    if (!skippedCharacters)
        return path;
    
    NSString *genericPath = [[NSString alloc] initWithCharacters:newPath length:newLength];
    return genericPath;
}

- (void) requestActor:(NSString *)path options:(NSDictionary *)options flags:(PriorityFlags)flags watcher:(id<DhtWatcher>)watcher {
    DhtHandle *handle = watcher.handler;
    dispatch_block_t requestBlock = ^ {
        // ignore any request from class have no delegate
        if (![handle hasDelegate]) {
            return ;
        }
        
        NSMutableDictionary *cancelTimers = _cancelRequestTimers;
        NSMutableDictionary *activeRequests = _activeRequests;
        NSString *genericPath = [self genericPathForParametrizedPath:path];
        NSMutableDictionary *requestInfo = nil;
        NSMutableDictionary *cancelRequestInfo = [cancelTimers objectForKey:path];
        
        //Need resume request corresponding with path is cancelling
        if (cancelRequestInfo != nil) {
            DhtRequestTimer *timer = [cancelRequestInfo objectForKey:@"timer"];
            [timer invalidate];
            timer = nil;
            requestInfo = [cancelRequestInfo objectForKey:@"requestInfo"];
            [activeRequests setObject:requestInfo forKey:path];
            [cancelTimers removeObjectForKey:path];
        }
        
        if (requestInfo == nil) {
            requestInfo = [activeRequests objectForKey:path];
        }
        
        if (requestInfo ==nil) {
            DhtActor *requestBuilder = [DhtActor requestBuilderForGenericPath:genericPath path:path];
            if (requestBuilder != nil) {
                NSMutableArray *watchers = [[NSMutableArray alloc] initWithObjects:handle, nil];
                
                requestInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                               requestBuilder, @"requestBuilder",
                               watchers, @"watchers",
                               nil];
                [activeRequests setObject:requestInfo forKey:path];
                
                [requestBuilder prepare:options];
                bool executeNow = true;
                
                /*
                 - This will check builder has queue name or not.
                 - If builder has queue name we will add to exist queue or create new queue. It helpful when one actor finish we will detect next request in current queue first
                 */
                if (requestBuilder.requestQueueName != nil) {
                    NSMutableArray *requestQueue = [_requestQueues objectForKey:requestBuilder.requestQueueName];
                    if (requestQueue == nil) {
                        requestQueue = [[NSMutableArray alloc] initWithObjects:requestBuilder, nil];
                        [_requestQueues setObject:requestQueue forKey:requestBuilder.requestQueueName];
                    } else {
                        [requestQueue addObject:requestBuilder];
                        if ([requestQueue count] > 1) {
                            executeNow = false;
                            if (flags & ActionChangePriority) {
                                if (requestQueue.count > 2) {
                                    [requestQueue removeLastObject];
                                    [requestQueue insertObject:requestBuilder atIndex:1];
                                    [KLogger log:@"(Inserted actor with high priority (next in queue)"];
                                }
                            }
                        }
                    }
                }
                
                if (executeNow) {
                    [requestBuilder execute:options];
                } else {
                    requestBuilder.storedOptions = options;
                }
            }
        } else {
            NSMutableArray *watchers = [requestInfo objectForKey:@"watchers"];
            
            if (![watchers containsObject:handle]) {
                [KLogger log:[NSString stringWithFormat:@"Joining watcher to the watchers of \"%@\"", path]];
                [watchers addObject:handle];
            } else {
                [KLogger log:[NSString stringWithFormat:@"Continue to watch for actor \"%@\"", path]];
            }
            
            DhtActor *actor = [requestInfo objectForKey:@"requestBuilder"];
            if (actor.requestQueueName == nil) {
                [actor watcherJoined:handle options:options waitingInActorQueue:false];
            } else {
                NSMutableArray *requestQueue = [_requestQueues objectForKey:actor.requestQueueName];
                if (requestQueue == nil || requestQueue.count == 0) {
                    [actor watcherJoined:handle options:options waitingInActorQueue:false];
                } else {
                    [actor watcherJoined:handle options:options waitingInActorQueue:[requestQueue objectAtIndex:0] != actor];
                    if (flags & ActionChangePriority)
                        [self changeActorPriority:path];
                }
            }
        }
        
    };
    requestBlock();
}

- (void)changeActorPriority:(NSString *)path {
    [self dispatchOnActionQueue:^{
         NSDictionary *requestInfo = [_activeRequests objectForKey:path];
         if (requestInfo != nil) {
             DhtActor *actor = [requestInfo objectForKey:@"requestBuilder"];
             if (actor.requestQueueName != nil) {
                 NSMutableArray *requestQueue = [_requestQueues objectForKey:actor.requestQueueName];
                 if (requestQueue != nil && requestQueue.count != 0) {
                     NSUInteger index = [requestQueue indexOfObject:actor];
                     if (index != NSNotFound && index != 0 && index != 1)
                     {
                         [requestQueue removeObjectAtIndex:index];
                         [requestQueue insertObject:actor atIndex:1];
                         [KLogger log:[NSString stringWithFormat:@"Changed actor %@ priority (next in %@)", path, actor.requestQueueName]];
                     }
                 }
             }
         }
     }];
}

- (void)removeWatcher:(id<DhtWatcher>)watcher {
    [self removeWatcherByHandle:[watcher handler]];
}

- (void)removeWatcherByHandle:(DhtHandle *)actionHandle {
    DhtHandle *watcherGraphHandle = actionHandle;
    if (watcherGraphHandle == nil) {
        [KLogger log:@"***** Warning:  handle is nil in removeWatcher"];
        return;
    }
    
    bool alreadyExecuting = false;
    OSSpinLockLock(&removeWatcherRequestsLock);
    if (!_removeWatcherRequests.empty()) {
        alreadyExecuting = true;
    }
    
    _removeWatcherRequests.push_back(watcherGraphHandle);
    OSSpinLockUnlock(&removeWatcherRequestsLock);
    
    if (alreadyExecuting && ![self isCurrentQueueActionQueue]) {
        return;
    }
    
    [self dispatchOnHighPriorityQueue:^ {
         std::vector<DhtHandle *> removeWatchers;
         
         OSSpinLockLock(&removeWatcherRequestsLock);
         removeWatchers.insert(removeWatchers.begin(), _removeWatcherRequests.begin(), _removeWatcherRequests.end());
         _removeWatcherRequests.clear();
         OSSpinLockUnlock(&removeWatcherRequestsLock);
         
         for (std::vector<DhtHandle *>::iterator it = removeWatchers.begin(); it != removeWatchers.end(); it++) {
             DhtHandle *actionHandle = *it;
             
             for (id key in [_activeRequests allKeys]) {
                 NSMutableDictionary *requestInfo = [_activeRequests objectForKey:key];
                 NSMutableArray *watchers = [requestInfo objectForKey:@"watchers"];
                 [watchers removeObject:actionHandle];
                 
                 if (watchers.count == 0) {
                     [self scheduleCancelRequest:(NSString *)key];
                 }
             }
             
             {
                 NSMutableArray *keysToRemove = nil;
                 for (NSString *key in [_liveNodeWatchers allKeys])
                 {
                     NSMutableArray *watchers = [_liveNodeWatchers objectForKey:key];
                     [watchers removeObject:actionHandle];
                     
                     if (watchers.count == 0)
                     {
                         if (keysToRemove == nil)
                             keysToRemove = [[NSMutableArray alloc] init];
                         [keysToRemove addObject:key];
                     }
                 }
                 if (keysToRemove != nil)
                     [_liveNodeWatchers removeObjectsForKeys:keysToRemove];
             }
             
             {
                 NSMutableArray *keysToRemove = nil;
                 for (NSString *key in [_actorMessagesWatchers allKeys])
                 {
                     NSMutableArray *watchers = [_actorMessagesWatchers objectForKey:key];
                     [watchers removeObject:actionHandle];
                     
                     if (watchers.count == 0)
                     {
                         if (keysToRemove == nil)
                             keysToRemove = [[NSMutableArray alloc] init];
                         [keysToRemove addObject:key];
                     }
                 }
                 if (keysToRemove != nil)
                     [_actorMessagesWatchers removeObjectsForKeys:keysToRemove];
             }
         }
     }];
}

- (void)removeWatcher:(id<DhtWatcher>)watcher fromPath:(NSString *)path {
    DhtHandle *handle = watcher.handler;
    [self removeWatcherByHandle:handle fromPath:path];
}

- (void)removeWatcherByHandle:(DhtHandle *)watcherGraphHandle fromPath:(NSString *)watcherPath
{
    if (watcherGraphHandle ==nil) {
        return;
    }
    
    bool alreadyExecuting = false;
    OSSpinLockLock(&removeWatcherFromPathRequestsLock);
    if (!_removeWatcherFromPathRequests.empty())
        alreadyExecuting = true;
    _removeWatcherFromPathRequests.push_back(std::pair<DhtHandle *, NSString *>(watcherGraphHandle, watcherPath));
    OSSpinLockUnlock(&removeWatcherFromPathRequestsLock);
    
    if (alreadyExecuting && ![self isCurrentQueueActionQueue])
        return;
    [self dispatchOnHighPriorityQueue:^{
        std::vector<std::pair<DhtHandle *, NSString *> > removeWatchersFromPath;
        OSSpinLockLock(&removeWatcherFromPathRequestsLock);
        removeWatchersFromPath.insert(removeWatchersFromPath.begin(), _removeWatcherFromPathRequests.begin(), _removeWatcherFromPathRequests.end());
        _removeWatcherFromPathRequests.clear();
        OSSpinLockUnlock(&removeWatcherFromPathRequestsLock);
        
        if (removeWatchersFromPath.size() > 1) {
            [KLogger log:[NSString stringWithFormat:@"Cancelled %ld requests at once", removeWatchersFromPath.size()]];
        }
        
        for (std::vector<std::pair<DhtHandle *, NSString *> >::iterator it = removeWatchersFromPath.begin(); it != removeWatchersFromPath.end(); it++) {
            DhtHandle *actionHandle = it->first;
            NSString *path = it->second;
            if (path == nil)
                continue;
            
            {
                NSMutableDictionary *requestInfo = [_activeRequests objectForKey:path];
                if (requestInfo != nil) {
                    NSMutableArray *watchers = [requestInfo objectForKey:@"watchers"];
                    if ([watchers containsObject:actionHandle]) {
                        [watchers removeObject:actionHandle];
                    }
                    if (watchers.count == 0) {
                        [self scheduleCancelRequest:(NSString *)path];
                    }
                }
            }
            
            {
                NSMutableArray *watchers = [_liveNodeWatchers objectForKey:path];
                if ([watchers containsObject:actionHandle]) {
                    [watchers removeObject:actionHandle];
                }
                if (watchers.count == 0) {
                    [_liveNodeWatchers removeObjectForKey:path];
                }
            }
            
            {
                NSMutableArray *watchers = [_actorMessagesWatchers objectForKey:path];
                if ([watchers containsObject:actionHandle]) {
                    [watchers removeObject:actionHandle];
                }
                if (watchers.count == 0) {
                    [_actorMessagesWatchers removeObjectForKey:path];
                }
            }
        }
    }];
}

- (void)scheduleCancelRequest:(NSString *)path {
    NSMutableDictionary *activeRequests = _activeRequests;
    NSMutableDictionary *cancelTimers = _cancelRequestTimers;
    
    NSMutableDictionary *requestInfo = [activeRequests objectForKey:path];
    NSMutableDictionary *cancelRequestInfo = [cancelTimers objectForKey:path];
    if (requestInfo != nil && cancelRequestInfo ==nil) {
        DhtActor *requestBuilder = [requestInfo objectForKey:@"requestBuilder"];
        NSTimeInterval cancelTimeout = requestBuilder.cancelTimeout;
        
        if (cancelTimeout <= DBL_EPSILON) {
            [activeRequests removeObjectForKey:path];
            [requestBuilder cancel];
            [KLogger log:[NSString stringWithFormat:@"Cancelled request to \"%@\"", path]];
            if (requestBuilder.requestQueueName != nil)
                [self removeRequestFromQueueAndProceedIfFirst:requestBuilder.requestQueueName fromRequestBuilder:requestBuilder];
        } else {
            [KLogger log: [NSString stringWithFormat:@"Will cancel request to \"%@\" in %f s", path, cancelTimeout]];
            NSDictionary *cancelDict = [NSDictionary dictionaryWithObjectsAndKeys:path, @"path", [NSNumber numberWithInt:0], @"type", nil];
            DhtRequestTimer *timer = [[DhtRequestTimer alloc] initWithTimeout:cancelTimeout repeat:false completion:^
                                      {
                                          [self performCancelRequest:cancelDict];
                                      } queue:[self globalStageDispatchQueue]];
            
            
            cancelRequestInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:requestInfo, @"requestInfo", nil];
            [cancelRequestInfo setObject:timer forKey:@"timer"];
            [cancelTimers setObject:cancelRequestInfo forKey:path];
            [activeRequests removeObjectForKey:path];
            [timer start];
        }
    }
}

- (void)performCancelRequest:(NSDictionary *)cancelDict {
    NSString *path = [cancelDict objectForKey:@"path"];
    
    [self dispatchOnActionQueue:^{
         NSMutableDictionary *cancelTimers = _cancelRequestTimers;
         
         NSMutableDictionary *cancelRequestInfo = [cancelTimers objectForKey:path];
         if (cancelRequestInfo == nil) {
             [KLogger log: [NSString stringWithFormat: @"Warning: cancelNodeRequestTimerEvent: \"%@\": no cancel info found", path]];
             return;
         }
         NSDictionary *requestInfo = [cancelRequestInfo objectForKey:@"requestInfo"];
         DhtActor *requestBuilder = [requestInfo objectForKey:@"requestBuilder"];
         if (requestBuilder == nil) {
             [KLogger log: [NSString stringWithFormat: @"Warning: active request builder for \"%@\" not fond, cannot cancel request", path]];
         } else {
             [requestBuilder cancel];
             [KLogger log: [NSString stringWithFormat: @"Cancelled request to \"%@\"", path]];
             if (requestBuilder.requestQueueName != nil)
                 [self removeRequestFromQueueAndProceedIfFirst:requestBuilder.requestQueueName fromRequestBuilder:requestBuilder];
         }
         [cancelTimers removeObjectForKey:path];
     }];
}

- (void)nodeRetrieveProgress:(NSString *)path progress:(float)progress {
    [self dispatchOnActionQueue:^{
        NSMutableDictionary *requestInfo = [_activeRequests objectForKey:path];
        if (requestInfo != nil) {
            NSMutableArray *watchers = [requestInfo objectForKey:@"watchers"];
            for (DhtHandle *handle in watchers) {
                id<DhtWatcher> watcher = handle.delegate;
                if (watcher != nil) {
                    if ([watcher respondsToSelector:@selector(actionNotifyProgress:progress:)]) {
                        [watcher actionNotifyProgress:path progress:progress];
                    }
                    if (handle.willReleaseOnMainThread) {
                        dispatch_async(dispatch_get_main_queue(), ^ { [watcher class]; });
                    }
                    watcher = nil;
                }
            }
        }
    }];
}

- (void)dispatchResource:(NSString *)path resource:(id)resource arguments:(id)arguments {
    [self dispatchOnActionQueue:^{
        NSArray *watchers = [_liveNodeWatchers objectForKey:path];
        if (watchers != nil) {
            for (DhtHandle *handle in watchers) {
                id<DhtWatcher> watcher = handle.delegate;
                if (watcher != nil) {
                    if ([watcher respondsToSelector:@selector(actionNotifyResourceDispatched:resource:arguments:)])
                        [watcher actionNotifyResourceDispatched:path resource:resource arguments:arguments];

                    if (handle.willReleaseOnMainThread)
                        dispatch_async(dispatch_get_main_queue(), ^ { [watcher class]; });
                    watcher = nil;
                }
            }
        }
    }];
}

- (bool)requestActorStateNow:(NSString *)path {
    if ([_activeRequests objectForKey:path] !=nil) {
        return true;
    }
    return false;
}
@end
