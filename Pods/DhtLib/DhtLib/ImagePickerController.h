//
//  ImagePickerController.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "ImagePickerAsset.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImagePickerCell.h"
#import "PostNewObject.h"
#import "DhtHandle.h"
#import "DhtWatcher.h"

#ifdef __cplusplus
extern "C" {
#endif
    
    void dispatchOnAssetsProcessingQueue(dispatch_block_t block);
    void sharedAssetsLibraryRetain();
    void sharedAssetsLibraryRelease();
    
#ifdef __cplusplus
}
#endif

@protocol ImagePickerControllerDelegate;
@interface ImagePickerController : UIViewController<DhtWatcher>
@property (nonatomic, strong) DhtHandle *handler;
- (id)initWithGroupUrl:(NSURL *)groupUrl groupTitle:(NSString *)groupTitle avatarSelection:(bool)avatarSelection;
+ (id)sharedAssetsLibrary;
+ (id)preloadLibrary;
+ (void)loadAssetWithUrl:(NSURL *)url completion:(void (^)(ALAsset *asset))completion;
+ (void)storeImageAsset:(NSData *)data;
@end

@protocol ImagePickerControllerDelegate <NSObject>

- (void)imagePickerController:(ImagePickerController *)imagePicker didFinishPickingWithAssets:(NSArray *)assets;
@end