//
//  ImagePickerCellCheckButton.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerCellCheckButton : UIButton

@property (nonatomic, strong) UIImageView *checkView;

- (void)setChecked:(bool)checked animated:(bool)animated;
- (bool)checked;
@end
