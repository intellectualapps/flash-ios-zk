//
//  DhtActionControl.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DhtActor.h"
#import "DhtHandle.h"
#import "DhtWatcher.h"

typedef NS_ENUM(NSInteger,PriorityFlags) {
    ActionRequestNormal = 0,
    ActionChangePriority = 1
};
@class DhtActionControl;
#ifdef __cplusplus
extern "C" {
#endif
    DhtActionControl *actionControlInstance();
    
#ifdef __cplusplus
}
#endif

@interface DhtActionControl : NSObject {
}
/*
 Note: This will pass task to the action queue and make the task execute in background thread.
 */
- (void)dispatchOnActionQueue:(dispatch_block_t)block;
/*
 Purpose : We can use this method to request any actor comfortable with the path
 Input:
 - path : this is identifier to detech which actor will execute this request. Please see the actor to see format of this path.
 - options : request options. This any key-value for actor to execute request. Check Specify Actor to know what is it.
 - flags : This flag will let action manager change the priority of request. 0 -  normal request will add to end of queue, 1 - is high request will execute immediately after current request finish.
 - watcher : This is delegate which will let action manager call back.
 */
- (void) requestActor:(NSString *)path options:(NSDictionary *)options flags:(PriorityFlags)flags watcher:(id<DhtWatcher>)watcher;
/*
 - Notify to destination class that the actor is complete
 - Auto check active request queue to find next request and execute it if need.
 */
- (void)changeActorPriority:(NSString *)path;
- (bool)requestActorStateNow:(NSString *)path;
- (void)actionCompleted:(NSString *)action result:(id)result error:(NSError *)error;
- (void)removeWatcher:(id<DhtWatcher>)watcher fromPath:(NSString *)path;
- (void)removeWatcher:(id<DhtWatcher>)watcher;
- (void)nodeRetrieveProgress:(NSString *)path progress:(float)progress;
- (void)dispatchResource:(NSString *)path resource:(id)resource arguments:(id)arguments;
@end
