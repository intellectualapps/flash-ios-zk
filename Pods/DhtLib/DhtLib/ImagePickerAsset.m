//
//  ImagePickerAsset.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "ImagePickerAsset.h"
#import "Utils.h"

@interface ImagePickerAsset() {
    __SYNCHRONIZED_DEFINE(_assetUrl);
}
@end


@implementation ImagePickerAsset

@synthesize asset = _asset;
@synthesize assetUrl = _assetUrl;
@synthesize thumbnailImage = _thumbnailImage;
@synthesize isLoading = _isLoading;
@synthesize isLoaded = _isLoaded;

- (id)initWithAsset:(ALAsset *)asset {
    self = [super init];
    if (self != nil) {
        __SYNCHRONIZED_INIT(_assetUrl);
        _asset = asset;
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self != nil) {
        __SYNCHRONIZED_INIT(_assetUrl);
    }
    return self;
}

- (id)copyWithZone:(NSZone *)__unused zone {
    ImagePickerAsset *copyAsset = [[ImagePickerAsset alloc] init];
    copyAsset.asset = _asset;
    copyAsset.assetUrl = _assetUrl;
    copyAsset.thumbnailImage = _thumbnailImage;
    copyAsset.isLoading = _isLoading;
    copyAsset.isLoaded = _isLoaded;
    return copyAsset;
}

- (void)setAsset:(ALAsset *)asset {
    _asset = asset;
}

- (NSString *)assetUrl {
    NSString *result = nil;
    __SYNCHRONIZED_BEGIN(_assetUrl);
    if (_assetUrl == nil && _asset != nil)
        _assetUrl = [[_asset.defaultRepresentation url] absoluteString];
    result = _assetUrl;
    __SYNCHRONIZED_END(_assetUrl);
    
    return _assetUrl;
}

- (void)load {
    _thumbnailImage = [[UIImage alloc] initWithCGImage:[_asset thumbnail]];
    _isLoading = false;
    _isLoaded = true;
}

- (void)unload {
    _thumbnailImage = nil;
    _isLoaded = false;
}

- (UIImage *)forceLoadedThumbnailImage {
    if (_thumbnailImage != nil)
        return _thumbnailImage;
    
    if (_asset != nil)
        return [[UIImage alloc] initWithCGImage:[_asset thumbnail]];
    
    return nil;
}
@end
