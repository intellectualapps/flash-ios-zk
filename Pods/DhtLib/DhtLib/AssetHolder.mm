//
//  AssetHolder.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "AssetHolder.h"
#import "ImagePickerAsset.h"
#import "PostNewObject.h"

AssetHolder *assetHolder(){
    static AssetHolder *singleton = nil;
    static dispatch_once_t onceToken;
    /*This will make this class thread safe access*/
    dispatch_once(&onceToken, ^
                  {
                      singleton = [[AssetHolder alloc] init];
                  });
    
    return singleton;
}

@interface AssetHolder () {
}

@end


@implementation AssetHolder

- (instancetype) init {
    self = [super init];
    if (self) {
        _selectedAssets = [[NSMutableArray alloc] init];
        _maxItems = 1;
    }
    return self;
}

- (BOOL) isFull {
    return (self.selectedAssets.count >= _maxItems);
}

- (void) executeCallback {
    if (self.getPhotoCompletion) {
        self.getPhotoCompletion(self.selectedAssets);
    }
}

- (void) clear {
    _maxItems = 1;
    if (_selectedAssets) {
        [_selectedAssets removeAllObjects];
    }
}

- (void) removeObjectAtIndex:(NSInteger)index {
    PostNewObject *postObj = nil;
    NSInteger tIndex = -1;
    for (PostNewObject *obj in self.selectedAssets) {
        tIndex ++;
        if (tIndex == index) {
            postObj = obj;
            break;
        }
    }
    if (postObj) {
        [self.selectedAssets removeObject:postObj];
    }
}

- (void) removeAssetWithUrl:(NSString *)url {
    PostNewObject *postObj = nil;
    for (PostNewObject *obj in self.selectedAssets) {
        if ([obj.assetUrl isEqualToString:url]) {
            postObj = obj;
            break;
        }
    }
    if (postObj) {
        [self.selectedAssets removeObject:postObj];
    }
}

- (BOOL) assetIsExistedWithUrl:(NSString *)url {
    for (PostNewObject *obj in self.selectedAssets) {
        if ([obj.assetUrl isEqualToString:url]) {
            return YES;
        }
    }
    return NO;
}

//
- (void) prepareForUse {
    //sort
    [self.selectedAssets sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        PostNewObject *p1 = (PostNewObject *)obj1;
        PostNewObject *p2 = (PostNewObject *)obj2;
        return [p1.createDate compare:p2.createDate];
    }];
}
@end
