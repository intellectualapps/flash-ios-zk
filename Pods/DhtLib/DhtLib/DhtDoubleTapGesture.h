//
//  DhtDoubleTapGesture.h
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/12/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DhtDoubleTapGesture : UIGestureRecognizer
@property (nonatomic) bool consumeSingleTap;
@property (nonatomic) bool doubleTapped;
@property (nonatomic) bool longTapped;
@property (nonatomic) bool avoidControls;

- (bool)canScrollViewStealTouches;
@end
@protocol DhtDoubleTapGestureDelegate <NSObject>

@optional

- (int)gestureRecognizer:(DhtDoubleTapGesture *)recognizer shouldFailTap:(CGPoint)point;
- (void)gestureRecognizer:(DhtDoubleTapGesture *)recognizer didBeginAtPoint:(CGPoint)point;
- (void)gestureRecognizerDidFail:(DhtDoubleTapGesture *)recognizer;
- (bool)gestureRecognizerShouldHandleLongTap:(DhtDoubleTapGesture *)recognizer;
- (void)doubleTapGestureRecognizerSingleTapped:(DhtDoubleTapGesture *)recognizer;
- (bool)gestureRecognizerShouldLetScrollViewStealTouches:(DhtDoubleTapGesture *)recognizer;
- (bool)gestureRecognizerShouldFailOnMove:(DhtDoubleTapGesture *)recognizer;
@end
