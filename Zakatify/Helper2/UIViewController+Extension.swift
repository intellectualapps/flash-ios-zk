//
//  UINavigationController+Extension.swift
//  Zakatify
//
//  Created by Thang Truong on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import FirebaseCore
// MARK: - contentViewController

extension UIViewController {
    var contentViewController: UIViewController {
        if let navContentVC = self as? UINavigationController {
            return navContentVC.visibleViewController!
        } else {
            return self
        }
    }

    class func logEvent(eventName: LogEventName, parameters: RequestParams? = nil) {
        if let parameters = parameters {
            var appeventDictionary: AppEvent.ParametersDictionary = [:]
            let dict = parameters.origin() as NSDictionary
            if let allKey = dict.allKeys as? [String] {
                for key in allKey {
                    if let value = dict.value(forKey: key) as?AppEventParameterValueType {
                        appeventDictionary[AppEventParameterName.custom(key)] = value
                    }
                }
            }
            print(eventName.rawValue)
            AppEventsLogger.log(eventName.rawValue, parameters: appeventDictionary, valueToSum: nil, accessToken: nil)
            Analytics.logEvent(eventName.rawValue, parameters: parameters.origin())
        } else {
            AppEventsLogger.log(eventName.rawValue)
            Analytics.logEvent(eventName.rawValue, parameters: nil)
        }
    }
}

enum LogEventName: String {
    case signupfacebookAndTwitter = "register_verified_event"
    case signupEmailOnly = "register_event"
    case connectToPaypalDone = "connect_paypal_account"
    case makeManualDonation = "manual_donation"
    case setZakatGoal = "set_zakat_goal"
    case selectAutoManualPayment = "select_automatic_manual_payment"
    case selectManualPaymentOnly = "select_manual_payment"
    case createPortfolio = "create_portfolio"
    case openApp = "open_app"
    case signupPreferences = "Singup_Preferences"
    case signupCalculator = "Signup_Calculator"
    case singupCalculatorBack = "Singup_calculator_back"
    case signupCalulate = "Signup_calulate"
    case singupDeductionTab = "Singup_deduction_tab"
    case signupPaypal = "Signup_paypal"
    case signupIntrest = "Signup_Intrest"
    case signupReady = "Signup_Ready"
    case openProfile = "Open_profile"
    case follow = "Follow"
    case search = "Search"
    case zakatifiersTab = "Zakatifiers_tab"
    case preferencesPreferences = "Preferences_Preferences"
    case preferencesCalculator = "Preferences_Calculator"
    case preferencesSave = "Preferences_save"

}
