//
//  AppFlyerHelper.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 3/20/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

enum AppFlyerEventType: Int {
    case appApp3 = 3 // 3
    case signup //4 => need
    case login //5 => need
    case manualDonation //6
    case manualDonation3 // 7
    case optIntoAutoDonation // 8 => need
    case connectPaypal // 9 => need
    case donationGoalSet // 10 => neeed
    case charityAdded // 11 => need
    case curatedFundAdded // 12
    case audoDonation // 13
    case donationMade //14

    func name() -> String {
        switch self {
        case .appApp3:
            return "App Opens 3+" //=> done
        case .signup:
            return "Sign Up" //=> done
        case .login:
            return "Login" //=> done
        case .manualDonation:
            return "Manual Donation" //=> done
        case .manualDonation3:
            return "Manual Donation 3+" //=> done
        case .optIntoAutoDonation:
            return "Opt in to Automatic Donation" //=> done
        case .connectPaypal:
            return "Connect Payment Method" //=> done
        case .donationGoalSet:
            return "Donation Goal Set" //=> done
        case .charityAdded:
            return "Charity Added" //=> done
        case .curatedFundAdded:
            return "Curated Fund Added" //=> done
        case .audoDonation:
            return "Automatic Donation"
        case .donationMade:
            return "Donation Made" //=> done
        }
    }
}
//4,5,8,9,10,11
class AppFlyerHelper: NSObject {

    class func track(type: AppFlyerEventType, values: [String: Any]? = nil) {
        let info = AutoDonationInfo.loadDonationInfo()
        if !info.isReady() {
            var changed = true
            switch type {
            case .optIntoAutoDonation:
                info.userOptsInAutoDonation = true
            case .connectPaypal:
                info.connectedPaypal = true
            case .donationGoalSet:
                info.setDonationGoal = true
            case .charityAdded:
                info.addCharityOrCurated = true
            default:
                changed = false
                break
            }
            if changed {
                info.save()
            }
            if info.isReady() {
                AppFlyerHelper.track(type: .audoDonation)
                AppFlyerHelper.track(type: .donationMade)
            }
        }
        AppsFlyerTracker.shared().trackEvent(type.name(),
                                             withValues: values);
    }
}
