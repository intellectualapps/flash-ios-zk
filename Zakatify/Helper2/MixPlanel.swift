//
//  MixPlanel.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 5/3/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

let mixpanelToken = "3df87517e5778d3c6eba7867eeb8dc09"

class ZMixPanelManager: NSObject {
    static let shared = ZMixPanelManager()
    let mixpanel = Mixpanel.mainInstance()
    override init() {
        super.init()
    }

    func track(name: String, properties: [String: MixpanelType], alsoSetPeopleProperties: Bool = false) {
        mixpanel.track(event: name, properties: properties)
        if properties.count > 0 && alsoSetPeopleProperties {
            mixpanel.people?.set(properties: properties)
        }
    }

    func trackFundAdded(name: String, tags: String, isCharity: Bool = false) {
        let model = UserManager.shared.sideMenuModel.value
        let count = model?.userStatus.userCount.portfolioCount ?? 0
        let properties: [String: MixpanelType] = [
            "Number of Charities in Portfolio": count + 1,
            "Name": name,
            "Tags": tags,
            "Categories": tags,
            "Charity or Fund?": isCharity ? "Charity" : "Fund"
        ]

        ZMixPanelManager.shared.track(name: "Fund Added", properties: properties)
    }
    
    func trackCreateUser(username: String, email: String) {
        if username.count > 0 {
            let properties = ["Username": username, "Email": email]
            self.track(name: "Username Created",
                       properties: properties,
                       alsoSetPeopleProperties: true)
        }
    }

    func trackVisitProfile(lastScreen: String) {
        self.track(name: "Profile Tapped", properties: ["Previous Screen": lastScreen])
    }

    func setPeopleProperty(name: String, value: MixpanelType) {
        Mixpanel.mainInstance().people?.set(properties: [name: value])
    }

    func trackCharityAdded(nbOfCharities: Int, name: String, tags: String, categories: String) {
        let properties: [String: MixpanelType] = [
            "Number of Charities in Portfolio": nbOfCharities,
            "Name": name,
            "Tags": tags,
            "Categories": categories
        ]
        ZMixPanelManager.shared.track(name: "Charity Added",
                                      properties: properties)
    }

    func trackFundTapped(previousScreen: String, name: String, tags: String, categories: String) {
        let properties: [String: MixpanelType] = [
            "Previous Screen": previousScreen,
            "Name": name,
            "Tags": tags,
            "Categories": tags
        ]
        self.track(name: "Fund Tapped", properties: properties)
    }
}
