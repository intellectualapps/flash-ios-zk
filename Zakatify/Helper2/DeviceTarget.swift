//
//  DeviceTarget.swift
//  Athlee-Onboarding
//
//  Created by mac on 07/07/16.
//  Updated by Matt J on 28/06/17
//  Copyright © 2017 Mosaic Finance Management. All rights reserved.
//

import UIKit

public struct DeviceTarget {
    public static let isIpad: Bool =  UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    public static let CURRENT_WIDTH: CGFloat = UIScreen.main.bounds.width
    public static let CURRENT_DEVICE: CGFloat = UIScreen.main.bounds.height
    public static let IS_LESSTHAN_OR_EQUAL_IP5 = UIScreen.main.bounds.height <= 568
    public static let IPHONE_4: CGFloat = 480
    public static let IPHONE_5: CGFloat = 568
    public static let IPHONE_6: CGFloat = 667
    public static let IPHONE_6_Plus: CGFloat = 736
    public static let IPHONE_X: CGFloat = 812

    public static let IS_IPHONE_4 = UIScreen.main.bounds.height == IPHONE_4
    public static let IS_IPHONE_5 = UIScreen.main.bounds.height == IPHONE_5
    public static let IS_IPHONE_6 = UIScreen.main.bounds.height == IPHONE_6
    public static let IS_IPHONE_6_Plus = UIScreen.main.bounds.height == IPHONE_6_Plus
    public static let IS_IPHONE_X = UIScreen.main.bounds.height >= IPHONE_X
}
