//
//  CharityDetailService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
class CharityDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var charity: Charity?
//        if let data = response as? Data {
//            let str = String.init(data: data, encoding: .utf8)
//            print(str)
//        }
        if let root = response as? [String: Any] {
            print(root)
            charity = Charity(JSON: root)
        }
        super.onFinish(charity, error: error, completion: completion)
    }
}
