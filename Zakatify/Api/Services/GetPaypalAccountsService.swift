//
//  GetPaypalAccountsService.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class GetPaypalAccountsService: ApiService {
    /* response format
     {
         "addNewPayPalAccountUrl":"https://flash-bff-and.appspot.com/add-paypal-account.jsp?token=Bearer eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjM4OTg0MjIsInN1YiI6ImJsYWNrIiwiaXNzIjoienVzIiwiZXhwIjoxNTU1NDU1Mzc0fQ.pEINz7UwvnIdtp8dRxQ23WtZ7wXiBzBs_ggrHDUg8Dg",
         "existingPayPalAccounts":[
             {
             "dateCreated":"2018-04-18T00:00:00",
             "id":"786d9uuusg4u29pei0avd290ph",
             "paypalEmail":"buls.getme@gmail.com",
             "paypalFirstName":"Bulama",
             "paypalLastName":"Yusuf",
             "status":"PRIMARY"
             }
         ]
     }
     */
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [PaypalAccount]()
        if let root = response as? [String: Any] {
            if let existingAccounts = root["existingPayPalAccounts"] as? [[String: Any]] {
                for accDict in existingAccounts {
                    let info = PaypalAccount(dict: accDict)
                    results.append(info)
                }
            }
            let url = root.stringOrEmptyForKey(key: "addNewPayPalAccountUrl")
            Defaults.addPaypalUrl.set(value: url)
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func getPaypalAccount(completion: @escaping NetworkServiceCompletion) {
        let path = Path.getPaypalAccounts.path
        let params = RequestParams()
        //2: is iOS
        params.setValue(2, forKey: "platform")
        let service = GetPaypalAccountsService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
