//
//  GetUserCharitiesListService.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/25/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class GetUserCharitiesListService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results: CharitiesInfo?
        if let root = response as? [String: Any] {
            results = CharitiesInfo(dict: root)
            if let charities = results?.charities {
                for var charity in charities {
                    charity.added = true
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func getUserCharitiesList(pageIndex: Int = 1, perpage: Int = 20, completion: @escaping NetworkServiceCompletion) {
        let username = UserManager.shared.currentUser?.username ?? ""
        let params = RequestParams()
        params.setValue(perpage, forKey: "page-size")
        params.setValue(pageIndex, forKey: "page-number")
        let path = Path.userCharities(username).path
        let service = GetUserCharitiesListService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
