//
//  AddOrDeleteCuratedFundService.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class AddOrDeleteCuratedFundService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        print(response)
        super.onFinish(response, error: error, completion: completion)
    }

    class func addCurated(fundid: Int, completion: @escaping NetworkServiceCompletion) {
        let username = UserManager.shared.currentUser?.username ?? ""
        let path = Path.userCurated(username).path
        let params = RequestParams()
        params.setValue(fundid, forKey: "fund-id")
        let service = AddOrDeleteCuratedFundService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }

    class func removeCurated(fundid: Int, completion: @escaping NetworkServiceCompletion) {
        let username = UserManager.shared.currentUser?.username ?? ""
        let path = Path.userCurated(username).path
        let params = RequestParams()
        params.setValue(fundid, forKey: "fund-id")
        let service = AddOrDeleteCuratedFundService(apiPath: path, method: .delete, requestParam: params, paramEncoding: Encoding.forMethod(method: .delete), retryCount: 1)
        service.doExecute(completion)
    }
}
