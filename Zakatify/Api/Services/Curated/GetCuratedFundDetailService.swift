//
//  GetCuratedFundDetailService.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class GetCuratedFundDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var curatedInfo: CuratedInfo?
        if let root = response as? [String: Any] {
            curatedInfo = CuratedInfo(JSON: root)
        }
        super.onFinish(curatedInfo, error: error, completion: completion)
    }
    
    class func getCuratedFundDetail(fundid: Int, completion: @escaping NetworkServiceCompletion ) {
        let path = Path.getCuratedFundDetail(fundid).path
        let params = RequestParams()
        let username = UserManager.shared.currentUser?.username ?? ""
        params.setValue(username, forKey: "username")
        let service = GetCuratedFundDetailService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
