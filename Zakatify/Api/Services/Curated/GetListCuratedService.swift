//
//  GetListCuratedService.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class GetListCuratedService: ApiService {
    var isOwnCurated = false
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [CuratedInfo]()
        if let root = response as? [String: Any] {
            if let curatedFunds = root["curatedFunds"] as? [[String: Any]] {
                curatedFunds.forEach { (dict) in
                    if let info = CuratedInfo(JSON: dict) {
                        if isOwnCurated {
                            info.selected = true
                        }
                        results.append(info)
                    }
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }
    
    class func getListCurated(pageIndex: Int = 1, perpage: Int = 20, completion: @escaping NetworkServiceCompletion) {
        let path = Path.listCurated.path
        let params = RequestParams()
        params.setValue(perpage, forKey: "page-size")
        params.setValue(pageIndex, forKey: "page-number")
        let service = GetListCuratedService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }

    class func getOwnListCurated(pageIndex: Int = 1, perpage: Int = 20, completion: @escaping NetworkServiceCompletion) {
        let username = UserManager.shared.currentUser?.username ?? ""
        let path = Path.userCurated(username).path
        let params = RequestParams()
        params.setValue(perpage, forKey: "page-size")
        params.setValue(pageIndex, forKey: "page-number")
        let service = GetListCuratedService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
        service.isOwnCurated = true
    }

}
