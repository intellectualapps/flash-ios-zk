//
//  CategoryServices.swift
//  Categoryify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result
import ObjectMapper


protocol UserCategoryServices {
    func add(categories:[Category], username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void)
    func get(username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void)
    func delete(category:Category, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}

class UserCategoryServicesCenter: UserCategoryServices {
    func add(categories:[Category], username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void) {
        let ids: String = categories.reduce("") { (result, category) -> String in
            if result.isEmpty {
                return "\(category.id)"
            }
            return result + ",\(category.id)"
        }
        
        let json: [String : Any] = ["category-id":ids]
        UserCategoryAPIProvider.request(UserCategoryAPI.add(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["categories"] as? [[String:Any]] {
                            let objects = Mapper<Category>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func get(username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void) {
        UserCategoryAPIProvider.request(UserCategoryAPI.get(username: username))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["categories"] as? [[String:Any]] {
                            let objects = Mapper<Category>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func delete(category:Category, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        let json = ["category-id":category.id,
                    "description":category.description] as [String : Any]
        UserCategoryAPIProvider.request(UserCategoryAPI.delete(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["state"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
