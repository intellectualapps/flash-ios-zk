//
//  MockServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol MockServices {
}

extension MockServices {
    var delayTime: Double {
        return 0.5
    }
    
    func delay(  complete:@escaping ()-> Void) {
        let triggerTime = self.delayTime
        DispatchQueue.main.asyncAfter(deadline: .now() + triggerTime, execute: {
            complete()
        })
    }
}
