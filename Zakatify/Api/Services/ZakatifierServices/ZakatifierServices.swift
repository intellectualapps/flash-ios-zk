//
//  UserServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result

/*
 UserServices use to make request to server 
 use :
 - mock
    let service:UserServices = UserServicesMock()
    don't like below
    let serviceMock:UserServicesMock = UserServicesMock()
 when run with real sever we only need change like below
    let service:UserServices = UserServicesReal() // or Any class implement UserServices
 */
protocol ZakatifierServices {
    func topMonthZakatifiers(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(zakatifiers:[UserPublicProfile],page:Int,size:Int,total:Int),NSError>)-> Void)
    func topZakatifiers(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(zakatifiers:[UserPublicProfile],page:Int,size:Int,total:Int),NSError>)-> Void)
    func userPublicProfile(username: String, complete:@escaping (_ result:Result<UserPublicProfile,NSError>)-> Void)
    func charityReviews(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(reviews:[CharityReview],page:Int,size:Int,total:Int),NSError>)-> Void) -> Cancellable
    func follow(username: String, followed: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
    func unfollow(username: String, followed: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)

}


class ZakatifierServicesCenter: ZakatifierServices {
    func charityReviews(username: String, page: Int, size: Int, complete: @escaping (Result<(reviews: [CharityReview], page: Int, size: Int, total: Int), NSError>) -> Void) -> Cancellable {
        return ZakatifierAPIProvider.request(.userReviews(user: username, page: page, size: size)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let reviews:[CharityReview] = try filtedResponce.mapArray(path: "reviews")
                    let page: Int = try filtedResponce.mapValue(path: "pageNumber")
                    let size: Int = try filtedResponce.mapValue(path: "pageSize")
                    let total: Int = try filtedResponce.mapValue(path: "totalCount")
                    let result = (reviews: reviews, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func follow(username: String, followed: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        ZakatifierAPIProvider.request(.follow(user: username, followed: followed)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let state: Bool = try filtedResponce.mapValue(path: "state")
                    complete(Result.success(state))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func unfollow(username: String, followed: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        ZakatifierAPIProvider.request(.unfollow(user: username, followed: followed)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let state: Bool = try filtedResponce.mapValue(path: "state")
                    complete(Result.success(state))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func topZakatifiers(username: String, page: Int, size: Int, complete: @escaping (Result<(zakatifiers: [UserPublicProfile], page: Int, size: Int, total: Int), NSError>) -> Void) {
        ZakatifierAPIProvider.request(.topZakatifiers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let zakatifiers:[UserPublicProfile] = try filtedResponce.mapArray(path: "topZakatifiers")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = zakatifiers.count
                    let result = (zakatifiers: zakatifiers, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func topMonthZakatifiers(username: String, page: Int, size: Int, complete: @escaping (Result<(zakatifiers: [UserPublicProfile], page: Int, size: Int, total: Int), NSError>) -> Void) {
        ZakatifierAPIProvider.request(.topMonthZakatifiers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let zakatifiers:[UserPublicProfile] = try filtedResponce.mapArray(path: "topZakatifiers")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = zakatifiers.count
                    let result = (zakatifiers: zakatifiers, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func userPublicProfile(username: String, complete: @escaping (Result<UserPublicProfile, NSError>) -> Void) {
        ZakatifierAPIProvider.request(.userDetails(user: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let profile:UserPublicProfile = try response.mapObject()
                    complete(Result.success(profile))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

}

