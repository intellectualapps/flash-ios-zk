//
//  UserServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result

/*
 UserServices use to make request to server 
 use :
 - mock
    let service:UserServices = UserServicesMock()
    don't like below
    let serviceMock:UserServicesMock = UserServicesMock()
 when run with real sever we only need change like below
    let service:UserServices = UserServicesReal() // or Any class implement UserServices
 */
protocol UserServices {
    
    /// function to check username is available
    ///
    /// - Parameters:
    ///   - username: username
    ///   - complete: complete handle
    /// - Returns: true if is available
    func verify(_ username:String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) -> Cancellable
    
    /// function to login
    ///
    /// - Parameters:
    ///   - userName: userName
    ///   - password: password
    ///   - complete: complete handle
    ///
    func login(_ userName:String, password: String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    /// function to login with facebook
    ///
    /// - Parameters:
    ///   - email: facebook email
    ///   - complete: complete handle
    /// - Returns:
    func loginFaceBook(_ email:String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    /// function to login with twitter
    ///
    /// - Parameters:
    ///   - email: facebook email
    ///   - complete: complete handle
    /// - Returns:
    func loginTwitter(_ email:String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    /// function to register
    ///
    /// - Parameters:
    ///   - userName: userName
    ///   - email: email to register
    ///   - password: password
    ///   - repeatPassword: repeat password
    ///   - complete: complete handle
    ///
    func register(_ userName:String,
                  email:String,
                  password: String,
                  repeatPassword: String,
                  complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    /// function to reset password
    ///
    /// - Parameters:
    ///   - email: email
    ///   - complete: complete handle
    func forgotPassword(_ email: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
    
    /// function to update user information to server
    ///
    /// - Parameters:
    ///   - userInfo: user info object
    ///   - complete: complete handle . When success return User with lastest information
    func update(_ userInfo:UserInfo, image:Data?, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    func createUser(_ userInfor:UserInfo, tempKey:String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void)
    
    func getSideMenuStatus(username:String, complete:@escaping (_ result:Result<SideMenuModel,NSError>)-> Void)

    func registerDeviceToken(username: String, deviceToken: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}


class UserServicesCenter: UserServices {
    deinit {
        print("UserServicesCenter ==> deinit")
    }
    /// function to check username is available
    ///
    /// - Parameters:
    ///   - username: username
    ///   - complete: complete handle
    /// - Returns: true if is available
    func verify(_ username: String, complete: @escaping (Result<Bool, NSError>) -> Void) -> Cancellable {
        return UserAPIProvider.request(UserAPI.verify(userName: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["status"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to login
    ///
    /// - Parameters:
    ///   - userName: userName
    ///   - password: password
    ///   - complete: complete handle
    ///
    func login(_ userName: String, password: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        UserAPIProvider.request(UserAPI.authenticate(id: userName,
                                                     password: password,
                                                     type: UserAPI.AuthenType.email.rawValue))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to login with facebook
    ///
    /// - Parameters:
    ///   - email: facebook email
    ///   - complete: complete handle
    /// - Returns:
    func loginFaceBook(_ email: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        UserAPIProvider.request(UserAPI.authenticate(id: email,
                                                     password: "",
                                                     type: UserAPI.AuthenType.facebook.rawValue))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to login with twitter
    ///
    /// - Parameters:
    ///   - email: facebook email
    ///   - complete: complete handle
    /// - Returns:
    func loginTwitter(_ email: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        UserAPIProvider.request(UserAPI.authenticate(id: email,
                                                     password: "",
                                                     type: UserAPI.AuthenType.twitter.rawValue))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to register
    ///
    /// - Parameters:
    ///   - userName: userName
    ///   - email: email to register
    ///   - password: password
    ///   - repeatPassword: repeat password
    ///   - complete: complete handle
    ///
    func register(_ userName: String, email: String, password: String, repeatPassword: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        UserAPIProvider.request(UserAPI.register(username: userName, email: email, password: password)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to reset password
    ///
    /// - Parameters:
    ///   - email: email
    ///   - complete: complete handle
    func forgotPassword(_ email: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        UserAPIProvider.request(UserAPI.forgotPassword(email: email)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _:UserInfo = try response.mapObject()
                    complete(Result.success(true))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    /// function to update user information to server
    ///
    /// - Parameters:
    ///   - userInfo: user info object
    ///   - complete: complete handle . When success return User with lastest information
    func update(_ userInfo: UserInfo, image:Data?, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        let json: [String:Any] = ["username":userInfo.username,
                                  "email":userInfo.email,
                                  "first-name":userInfo.firstName,
                                  "last-name":userInfo.lastName,
                                  "location":userInfo.location,
                                  "phone-number":userInfo.mobile,
                                  "photo-url":userInfo.profileUrl,
                                  "facebook-email":userInfo.facebookEmail,
                                  "twitter-email":userInfo.twitterEmail]
        
        UserAPIProvider.request(UserAPI.updateUser(json: json)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func createUser(_ userInfo:UserInfo, tempKey:String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void) {
        var json = ["username":userInfo.username,
                    "email":userInfo.email,
                    "first-name":userInfo.firstName,
                    "last-name":userInfo.lastName,
                    "location":userInfo.location,
                    "phone-number":userInfo.mobile,
                    "photo-url":userInfo.profileUrl,
                    "facebook-email":userInfo.facebookEmail,
                    "twitter-email":userInfo.twitterEmail] as [String:Any]
        json["temp-key"] = tempKey
        print(json)
        UserAPIProvider.request(UserAPI.createUser(json: json)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let user:UserInfo = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func getSideMenuStatus(username:String, complete:@escaping (_ result:Result<SideMenuModel,NSError>)-> Void) {
        UserAPIProvider.request(UserAPI.sideMenu(user: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let user:SideMenuModel = try response.mapObject()
                    complete(Result.success(user))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func registerDeviceToken(username: String, deviceToken: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        UserAPIProvider.request(.deviceToken(deviceToken: deviceToken, username: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try response.filterSuccessfulStatusCodes()
                    complete(Result.success(true))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}


class UserServicesMock: UserServices, MockServices {
    
    func verify(_ username: String, complete: @escaping (Result<Bool, NSError>) -> Void) -> Cancellable {
        return UserAPIProvider.request(UserAPI.verify(userName: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["status"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func login(_ userName: String, password: String, complete:@escaping (Result<UserInfo, NSError>) -> Void) {
        let user = UserInfo()
        user.username = userName
        delay {
            complete(Result.success(user))
        }
    }
    
    func loginFaceBook(_ email: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        let user = UserInfo()
        user.username = ""
        delay {
            complete(Result.success(user))
        }
    }
    
    func loginTwitter(_ email: String, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        let user = UserInfo()
        user.username = ""
        delay {
            complete(Result.success(user))
        }
    }
    
    func register(_ userName: String,
                  email: String,
                  password: String,
                  repeatPassword: String,
                  complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        let user = UserInfo()
        user.username = userName
        user.email = email
        delay {
            complete(Result.success(user))
        }
    }
    
    func forgotPassword(_ email: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        delay {
            complete(Result.success(true))
        }
    }
        
    func update(_ userInfo: UserInfo, image:Data?, complete: @escaping (Result<UserInfo, NSError>) -> Void) {
        delay {
            complete(Result.success(userInfo))
        }
    }
    
    func createUser(_ userInfor:UserInfo, tempKey:String, complete:@escaping (_ result:Result<UserInfo,NSError>)-> Void) {
        delay {
            complete(Result.success(userInfor))
        }
    }
    
    func getSideMenuStatus(username:String, complete:@escaping (_ result:Result<SideMenuModel,NSError>)-> Void) {
        
    }

    func registerDeviceToken(username: String, deviceToken: String, complete: @escaping (Result<Bool, NSError>) -> Void) {

    }
}
