//
//  CharityListPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol CharityListViewPresenter: TableViewPresenter {
    var charities: Variable<[CharityDetail]> {get set}
    var zakatifierSearchPresenter: ListZakatifiersPresenter? {get set}
    var view:TableView {get}
    init(view: TableView)
    func charity(index:Int) -> CharityDetail?
    func add(charity:CharityDetail, completion: @escaping (Int)->())
    func remove(charity:CharityDetail)
    func loadmoreSearch(keyword: String?)
}

class CharityListPresenter: CharityListViewPresenter {
    var charities: Variable<[CharityDetail]> = Variable([CharityDetail]())
    var zakatifierSearchPresenter: ListZakatifiersPresenter?
    unowned var view: TableView
    var pageCount: Page = PageCount()
    var pageSearch: Page = PageCount()
    let service: CharityServices = CharityServicesCenter()
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    var charityViewModel = CharityViewModel()
    required init(view: TableView) {
        self.view = view
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.added.rawValue),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.remove.rawValue),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return charities.value.count
    }
    
    @objc func notificationChange() {
        view.refreshed()
    }
    
    var searchCancelable: Cancellable?
    func refresh(search: String? = nil) {
        guard let search = search else {
            return
        }
        if search.isEmpty {
            return
        }
        if searchCancelable?.cancel() != nil {
            view.refreshed()
        }
        view.refreshing()
        charityViewModel.charitySearch(username: username, keyword: search, page: pageSearch.page, size: pageSearch.size) {  [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let dict = response as? [String:Any] {
                    if let charitiesInfo = dict["charities"] as? CharitiesInfo {
                        strongSelf.charities.value = charitiesInfo.charities
                        strongSelf.pageSearch = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                        if search.count > 0 {
                            let params = RequestParams()
                            params.setValue(search, forKey: "Search term")
                            params.setValue(String(charitiesInfo.charities.count), forKey: "Charities_Results_number")
                            params.setValue(String(charitiesInfo.charities.count), forKey: "Total_Results")
                            UIViewController.logEvent(eventName: .search, parameters: params)
                        }
                    }
                    strongSelf.zakatifierSearchPresenter?.zakatifiers = []
                    if let zakatifiersInfo = dict["zakatifiers"] as? [ZakatifierInfo] {
                        for zakatifierInfo in zakatifiersInfo {
                            if let zakaProfile = UserPublicProfile(JSON: [:]) {
                                zakaProfile.user = zakatifierInfo
                                strongSelf.zakatifierSearchPresenter?.zakatifiers.append(zakaProfile)
                            }
                        }
                    }
                }
                strongSelf.view.refreshed()
            }
        }
    }
    
    func refresh() {
        view.refreshing()
        charityViewModel.getCharitiesListl(username: username, page: 1, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let charitiesInfo = response as? CharitiesInfo {
                    strongSelf.charities.value = charitiesInfo.charities
                    strongSelf.pageCount = PageCount(page: 1, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.view.refreshed()
            }
        }
    }

    func getOwnCharities(isReload: Bool = true, completion: NetworkServiceCompletion) {
        let pageIndex = isReload ? 1 : (self.charities.value.count / 20 + 1)
        view.refreshing()
        GetUserCharitiesListService.getUserCharitiesList(pageIndex: pageIndex, perpage: 20) {[weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()

            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {

                if let charitiesInfo = response as? CharitiesInfo {
                    if isReload {
                        strongSelf.charities.value = charitiesInfo.charities
                    } else {
                        var temp = strongSelf.charities.value
                        var ids = temp.map({$0.id})
                        for charity in charitiesInfo.charities {
                            if !ids.contains(charity.id) {
                                ids.append(charity.id)
                                temp.append(charity)
                            }
                        }
                        strongSelf.charities.value = temp
                    }
                }
                strongSelf.view.refreshed()
            }
        }
    }
    
    func loadmoreSearch(keyword: String?) {
        guard let search = keyword else {
            return
        }
        if search.isEmpty {
            return
        }
        if searchCancelable?.cancel() != nil {
            view.refreshed()
        }
        view.refreshing()
        charityViewModel.charitySearch(username: username, keyword: search, page: pageSearch.page + 1, size: pageSearch.size) {  [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                var tmp = strongSelf.charities.value
                if let charitiesInfo = response as? CharitiesInfo {
                    let _charities = charitiesInfo.charities
                    tmp.append(contentsOf: _charities)
                    strongSelf.pageSearch = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.charities.value = tmp
                strongSelf.view.refreshed()
            }
        }
    }
    
    func loadmore() {
        if shouldLoadmore() == false {
            return
        }
        view.refreshing()
        charityViewModel.getCharitiesListl(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                var tmp = strongSelf.charities.value
                if let charitiesInfo = response as? CharitiesInfo {
                    tmp.append(contentsOf: charitiesInfo.charities)
                    strongSelf.pageCount = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.charities.value = tmp
                strongSelf.view.refreshed()
            }
        }
    }
    
    func shouldLoadmore() -> Bool {
        return true
    }
    
    func charity(index: Int) -> CharityDetail? {
        if index < charities.value.count {
            return charities.value[index]
        }
        return nil
    }

    func removeCharityFromList(charity: CharityDetail) {
        let filter = charities.value.filter({$0.id != charity.id})
        charities.value = filter
        self.view.refreshed()
    }

    func addChartityToList(charity: CharityDetail) {
        var temp = charities.value
        let first = temp.filter({$0.id == charity.id}).first
        if first == nil {
            temp.append(charity)
            self.view.refreshed()
        }
    }
    
    func add(charity:CharityDetail, completion: @escaping (Int) -> ()) {
        let index = charities.value.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.addToPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success( _):
                if let index = index {
                    self?.charities.value[index].added = true
                }
                AppFlyerHelper.track(type: .charityAdded)
                strongSelf.view.refreshed()
                completion(self?.charities.value.count ?? 0)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
                completion(self?.charities.value.count ?? 0)
            }
            strongSelf.view.refreshed()
        }
    }
    
    func remove(charity:CharityDetail) {
        let index = charities.value.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.removeFromPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success( _):
                if let index = index {
                    self?.charities.value[index].added = false
                    self?.charities.value.remove(at: index)
                }
                strongSelf.view.refreshed()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
}
