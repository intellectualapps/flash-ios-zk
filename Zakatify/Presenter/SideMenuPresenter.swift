//
//  SideMenuPresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol UserStatusView: CommonView {
    func fillData()
}

protocol SideMenuViewPresenter {
    var sideMenuModel: SideMenuModel {get set}
    var view: UserStatusView {get set}
    //
    var avartarURL: URL? {get}
    var username: String {get}
    var percentCompleted: Double {get}
    var nameAttributedString: NSAttributedString {get}
    var totalDonation: Double {get}
    var goal: Float {get}
    var rank: Int {get}
    var points: Int {get}
    var rankDescriptionAttributedString: NSAttributedString {get}
    var notification: Int {get}
    var message: Int {get}
    //
    init(view: UserStatusView)
    func refresh()
    func didReceiveModel(model: SideMenuModel)
}

extension SideMenuViewPresenter {
    var avartarURL: URL? {
        return sideMenuModel.user.avartarURL
    }
    var username: String {
        return sideMenuModel.user.username
    }
    var percentCompleted: Double {
        return sideMenuModel.userStatus.donationProgress.percentCompleted
    }
    var nameAttributedString: NSAttributedString {
        return sideMenuModel.user.nameAttributedString
    }
    var totalDonation: Double {
        return sideMenuModel.userStatus.donationProgress.totalDonation
    }
    var goal: Float {
        return sideMenuModel.userStatus.donationProgress.goal
    }
    var rank: Int {
        return sideMenuModel.userStatus.userBoard.rank
    }
    var points: Int {
        return sideMenuModel.userStatus.userBoard.points
    }
    var rankDescriptionAttributedString: NSAttributedString {
        return sideMenuModel.userStatus.userBoard.descriptionAttributedString
    }
    var notification: Int {
        return sideMenuModel.userStatus.notification.count
    }
    var message: Int {
        return sideMenuModel.userStatus.message.count
    }
}

class SideMenuPresenter: SideMenuViewPresenter {
    var sideMenuModel: SideMenuModel = SideMenuModel() {
        didSet {
            sideMenuModel.save()
            view.fillData()
        }
    }
    var view: UserStatusView
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    private let sevices: UserServices = UserServicesCenter()
    required init(view: UserStatusView) {
        self.view = view
        if let saved = SideMenuModel.load() {
            self.sideMenuModel = saved
        } else if let user = UserManager.shared.currentUser {
            self.sideMenuModel.user = user
        }
    }
    func refresh() {
        sevices.getSideMenuStatus(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let model):
                UserManager.shared.sideMenuModel.value = model
                strongSelf.didReceiveModel(model: model)
            case .failure(_):
//                strongSelf.view.showAlert(error.localizedDescription)
                break
            }
        }
    }

    func didReceiveModel(model: SideMenuModel) {

        self.sideMenuModel = model
        let currentUser = UserManager.shared.currentUser
        currentUser?.portfolios = model.userStatus.userCount.portfolioCount
        currentUser?.followers = model.userStatus.userCount.followerCount
        currentUser?.donations = model.userStatus.userCount.donationCount
        currentUser?.badges = model.userStatus.notification.count
        UserManager.shared.currentUser = currentUser
    }
}
