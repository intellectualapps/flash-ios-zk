//
//  UIApplication+Ext.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 10/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension UIApplication {
    func connectOrCreatePaypalAccount() {
        let url = Defaults.addPaypalUrl.getStringOrEmpty()
        if url.lenght > 0 {
            guard let newurl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
                return
            }
            if let aurl = URL(string: newurl) {
                UIApplication.shared.open(aurl, options: [:]) { (success) in

                }
            }
        }
    }
}
