//
//  UIFontExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import QuartzCore
import CoreText
import CoreGraphics
import UIKit

extension UIFont {

    class func googlePlayFontBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Play-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }

    class func googlePlayFontRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Play-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
    }

    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    class func loadCustomFont() {
        var paths = [String]()
        let fontFileNames = ["Play-Bold.ttf", "Play-Regular.ttf"]
        for name in fontFileNames {
            let filename = (name as NSString).deletingPathExtension
            let fExtension = (name as NSString).pathExtension
            if let path = Bundle.main.path(forResource: filename, ofType: fExtension) {
                paths.append(path)
            }
        }
        loadCustomFont(filePaths: paths)
    }

    class func loadCustomFont(filePaths: [String]) {
        for path in filePaths {
            if FileManager.default.fileExists(atPath: path) {
                let url = URL(fileURLWithPath: path)
                do {
                    let data = try Data(contentsOf: url)
                    if let provider = CGDataProvider(data: data as CFData) {
                        let font = CGFont(provider)
                        CTFontManagerRegisterGraphicsFont(font ?? UIFont.systemFont(ofSize: 14) as! CGFont, nil)
                    }
                } catch {

                }
            }
        }
    }
}
