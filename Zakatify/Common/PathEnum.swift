//
//  PathEnum.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
private struct Constants {
    static let baseUrl = Enviroment.env(dev: "https://flash-bff-ios.appspot.com/api/v1/",
                                        stg: "https://flash-bff-ios.appspot.com/api/v1/",
                                        prod: "https://flash-bff-ios.appspot.com/api/v1/")
}

enum Path {
    case getCuratedFundDetail(Int)
    case userCurated(String)
    case listCurated
    case menu(String)
    case Login
    case loginFacebook
    case getPaypalAccounts
    case charitySearch
    case zakatifierSearch
    case charityList(String)
    case chartityReview(Int)
    case charityDetail(Int)
    case paypalDelete(String)
    case paypalGet(String)
    case paypalSetPrimary(String)
    case userCharities(String)
    var relativePath: String {
        switch self {
        case .userCharities(let name):
            return "preference/user-charity/\(name)"
        case .getCuratedFundDetail(let id):
            return "curated-fund/\(id)"
        case .userCurated(let username):
            return "preference/user-curated-fund/\(username)"
        case .listCurated:
            return "curated-fund"
        case .menu(let username):
            return "user/menu/\(username)"
        case .getPaypalAccounts:
            return "preference/paypal-accounts"
        case .Login:
            return ""
        case .charitySearch:
            return "charity/search"
        case .zakatifierSearch:
            return "zakatifier/search"
        case .charityList(let username):
            return "charity/suggest/\(username)"
        case .chartityReview(let id):
            return "review/charity/\(id)"
        case .charityDetail(let id):
            return "charity/details/\(id)"
        case .paypalDelete(let id):
            return "payment/remove/\(id)"
        case .paypalGet(let username):
            return "preference/user-payment-option/\(username)"
        case .paypalSetPrimary(let id):
            return "preference/user-payment-option/primary/\(id)"
        case .loginFacebook:
            return "user/authenticate"
        }
    }

    var path: String {
        return (NSURL(string: Constants.baseUrl)?.appendingPathComponent(relativePath)?.absoluteString) ?? ""
    }
}
