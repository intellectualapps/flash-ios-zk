//
//  UserDefault.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

enum Defaults: String {
    case numberOfMessageSent = "numberOfMessageSent"
    case userid = "userid"
    case username = "username"
    case permissionLevel = "permissionLevel"
    case host = "host"
    case password = "password"
    case port = "port"
    case heartBeatEnabled = "heartBeat"
    case addPaypalUrl = "addPaypalUrl"
    case manualDonationCount = "manualDonationCount"
    case openAppCount = "openAppCount"
    case numberOfSearch = "numberOfSearch"
    case numberOfDonateTap = "numberOfDonateTap"
    static func increaseOpenAppCount() {
        var count = Defaults.openAppCount.getInt() ?? 0
        count += 1
        Defaults.openAppCount.set(value: count)
    }
    static func increaseDonationCount() {
        var count = Defaults.manualDonationCount.getInt() ?? 0
        count += 1
        Defaults.manualDonationCount.set(value: count)
    }

    func set(value: Any?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }

    func get() -> Any? {
        return UserDefaults.standard.object(forKey: self.rawValue)
    }

    func getBool() -> Bool {
        return UserDefaults.standard.bool(forKey: self.rawValue)
    }

    func getString() -> String? {
        return UserDefaults.standard.string(forKey: self.rawValue)
    }

    func getStringOrEmpty() -> String {
        return self.getString() ?? ""
    }

    func getIntWithDefault() -> Int {
        return getInt() ?? 0
    }
    
    func getInt() -> Int? {
        return UserDefaults.standard.integer(forKey: self.rawValue)
    }

    static func clear() {
        Defaults.userid.set(value: nil)
        Defaults.username.set(value: nil)
        Defaults.permissionLevel.set(value: nil)
    }

    static func persist() {
        //Do not load default preference if did before
        if let host = Defaults.host.getString(), host.lenght > 0 {
            return
        }
        if let url = Bundle.main.url(forResource: "DefaultPreferences", withExtension: "plist") {
            if let defaultPrefs = NSDictionary(contentsOf: url) as? [String: Any] {
                UserDefaults.standard.register(defaults: defaultPrefs)
                Defaults.host.set(value: defaultPrefs.stringForKey(key: Defaults.host.rawValue))
                Defaults.port.set(value: defaultPrefs.intForKey(key: Defaults.port.rawValue))
                Defaults.password.set(value: defaultPrefs.stringForKey(key: Defaults.password.rawValue))
                Defaults.heartBeatEnabled.set(value: defaultPrefs.stringForKey(key: Defaults.heartBeatEnabled.rawValue))
            }
        }
    }
}
