//
//  CircleView.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CircleView: UIView {
    @IBInspectable var lineWidth: CGFloat = 12
    @IBInspectable var value:CGFloat = 70.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var chartStartAngle: CGFloat = CGFloat.pi/2.0 + CGFloat.pi / 8.0
    @IBInspectable var chartEndAngle: CGFloat = CGFloat.pi*2 + CGFloat.pi/2.0 - CGFloat.pi / 8.0
    
    let margin = CGFloat.pi / 8.0
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawLineBG(rect)
        drawLine(rect)
    }
    
    private func drawLineBG(_ rect: CGRect) {
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext();
        
        // Set the circle outerline-width
        context!.setLineWidth(lineWidth);
        
        // Set the circle outerline-colour
        UIColor.lightGray.set()
        
        // Create Circle
        context?.addArc(center: CGPoint(x: rect.width/2.0, y: rect.size.height/2.0) , radius: rect.size.width/2.0 - lineWidth, startAngle: chartStartAngle, endAngle: chartEndAngle, clockwise: false)
        context!.setLineCap(CGLineCap.round)
        // Draw
        context!.strokePath()
    }
    
    private func drawLine(_ rect: CGRect) {
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext();
        
        // Set the circle outerline-width
        context!.setLineWidth(lineWidth);
        
        // Set the circle outerline-colour
        if value < 100 {
            UIColor.green.set()
        } else {
            UIColor.blue.set()
        }
        
        // Create Circle
        let endAngle = chartStartAngle + (chartEndAngle - chartStartAngle)*value/100.0
        
        context?.addArc(center: CGPoint(x: rect.width/2.0, y: rect.size.height/2.0) , radius: rect.size.width/2.0 - lineWidth, startAngle: chartStartAngle, endAngle: endAngle, clockwise: false)
        context!.setLineCap(CGLineCap.round)
        // Draw
        context!.strokePath()
    }
}
