//
//  CuratedDetailViewModel.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CuratedDetailViewModel: CuratedViewModel {
    var listViewModel: CuratedViewModel?
    var curatedInfo: Variable<CuratedInfo?> = Variable(nil)
    var fundId: Int {
        get {
            return curatedInfo.value?.fundId ?? -1
        }
    }

    func refresh(completion: @escaping NetworkServiceCompletion) {
        GetCuratedFundDetailService.getCuratedFundDetail(fundid: fundId) {[weak self] (response, error) in
            if let info = response as? CuratedInfo {
                self?.curatedInfo.value = info
            }
            completion(response, error)
        }
    }
}
