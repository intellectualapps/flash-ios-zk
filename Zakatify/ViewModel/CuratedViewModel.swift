//
//  CuratedViewModel.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CuratedViewModel: NSObject {
    var listCurated = Variable([CuratedInfo]())

    func addCurated(info: CuratedInfo) {
        var temp = listCurated.value
        let fist = temp.filter({$0.fundId == info.fundId}).first
        if fist == nil{
            temp.append(info)
        }
        listCurated.value = temp
    }

    func removeCurated(id: Int) {
        let values = listCurated.value.filter({$0.fundId != id})
        listCurated.value = values
    }

    func getCuratedList(isReload: Bool = true, completion: (NetworkServiceCompletion)? = nil) {
        let pageindex = isReload ? 1 : (listCurated.value.count / 2 + 1)
        GetListCuratedService.getListCurated(pageIndex: pageindex, perpage: 20) {[weak self] (response, error) in
            if let list = response as? [CuratedInfo] {
                self?.didReceiveNewListCurated(newList: list, isReload: isReload)
            }
            completion?(response, error)
        }
    }

    func getOwnCuratedList(isReload: Bool = true, completion: (NetworkServiceCompletion)? = nil) {
        let pageindex = isReload ? 1 : (listCurated.value.count / 2 + 1)
        GetListCuratedService.getOwnListCurated(pageIndex: pageindex, perpage: 20) { [weak self] (response, error) in
            if let list = response as? [CuratedInfo] {
                self?.didReceiveNewListCurated(newList: list, isReload: isReload)
            }
            completion?(response, error)
        }
    }

    fileprivate func didReceiveNewListCurated(newList: [CuratedInfo], isReload: Bool = true) {
        if isReload {
            self.listCurated.value = newList
        } else {
            var currentList = listCurated.value
            var ids = currentList.map({$0.fundId})
            newList.forEach { (info) in
                if !ids.contains(info.fundId) {
                    currentList.append(info)
                    ids.append(info.fundId)
                }
            }
            listCurated.value = currentList
        }
    }

    func removeCuratedFund(fundid: Int, completion: @escaping NetworkServiceCompletion) {
        AddOrDeleteCuratedFundService.removeCurated(fundid: fundid, completion: completion)
    }

    func addToPortfolio(fundid: Int, completion: @escaping NetworkServiceCompletion) {
        AddOrDeleteCuratedFundService.addCurated(fundid: fundid, completion: completion)
    }
}
