//
//  ViewController.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var facebookBtn: Button!
    @IBOutlet weak var twitterBtn: Button!
    @IBOutlet weak var emailBtn: Button!
    @IBOutlet weak var whyZakatifyBtn: Button!
    
    fileprivate var viewModel = LoginViewModel()
    let service: UserServices = UserServicesCenter()

    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.whyZakatifyBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.whyZakatifyBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        self.facebookBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.facebookBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        self.twitterBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.twitterBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        self.emailBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.emailBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func privateBtnAction(_ sender: Any) {
        openWebview(type: .privacy, title: "Privacy", previousScreen: "Onboarding")
    }
    
    @IBAction func aboutBtnAction(_ sender: Any) {

        openWebview(type: .about, title: "About", previousScreen: "Onboarding")
    }
    
    @IBAction func onTapTermsAction(_ sender: UIButton) {
        openWebview(type: .terms, title: "Terms", previousScreen: "Onboarding")
    }
    
    @IBAction func onClickFacebook() {
        ZMixPanelManager.shared.track(name: "Facebook Login/Register Tapped", properties: [:])
        loginFacebook()
        UIViewController.logEvent(eventName: LogEventName.signupfacebookAndTwitter)
        AppFlyerHelper.track(type: AppFlyerEventType.signup)
    }
    
    @IBAction func onClickTwitter() {
        ZMixPanelManager.shared.track(name: "Twitter Login/Register Tapped", properties: [:])
        loginTwitter()
        UIViewController.logEvent(eventName: LogEventName.signupfacebookAndTwitter)
        AppFlyerHelper.track(type: AppFlyerEventType.signup)
    }
    
    
    // TODO : assign a.Thang
    func gotoLoginByTwitter(user:UserInfo) {
        user.fromFacebook = false
        performSegue(withIdentifier: "gotoCreateUser", sender: user)

    }
    // TODO : assign a.Thang
    func gotoLoginByFacebook(user:UserInfo) {
        user.fromFacebook = true
        performSegue(withIdentifier: "gotoCreateUser", sender: user)
    }
    // TODO : assign Minh
    
    func gotoHome() {
        DispatchQueue.main.async {
            AppDelegate.shareInstance().gotoHomeViewController()
        }
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case "gotoCreateUser":
            if let navagation = segue.destination as? UINavigationController {
                if let createUserVC = navagation.viewControllers.first as? CreateUserViewController, let user = sender as? UserInfo {
                    createUserVC.user = user
                }
            }
        case "loginEmail":
            ZMixPanelManager.shared.track(name: "Email Login/Register Tapped", properties: [:])
        default:
            return
        }
    }
    
    func loginFacebook() {
        if let _ = FBSDKAccessToken.current() {
            getFacebookEmail()
            return
        }
        let manager = FBSDKLoginManager()
        manager.logIn(withReadPermissions: ["public_profile","email"], from: self) { [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result {
                if result.isCancelled {
                    
                } else {
                    strongSelf.getFacebookEmail()
                }
            }
        }
    }
    
    func getFacebookEmail() {
        guard let _ = FBSDKAccessToken.current() else {
            return
        }
        let params = ["fields":"id,name,email"]
        view.showLoading()
        FBSDKGraphRequest(graphPath: "me", parameters: params).start { [weak self] (connection, result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideLoading()
            strongSelf.view.hideLoading()
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result as? [String:Any] {
                print(result)
                if let email = result["email"] as? String {
                    strongSelf.loginFacebook(email: email)
                }
            }
        }
    }
    
    func loginFacebook(email:String) {
        view.showLoading()
        
        viewModel.loginFacebook(email: email) { [weak self] (response, error) in
            self?.hideLoading()
            if let error = error {
                self?.showAlert(error.description)
            }
            if let user = response as? UserInfo {
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                if user.username.isEmpty {
                    self?.gotoLoginByFacebook(user: user)
                } else {
                    self?.gotoHome()
                }
            }
        }
    }
    
    func loginTwitter() {
        let client = TWTRAPIClient.withCurrentUser()
        if let _ = client.userID {
            getTwitterEmail()
            return
        }
        TWTRTwitter.sharedInstance().logIn(completion: {[weak self] (session, error) in
            guard let strongSelf = self else {
                return
            }
            if (session != nil) {
                strongSelf.getTwitterEmail()
                ZMixPanelManager.shared.track(name: "Sign In", properties: ["Sign In Method": "Twitter"])
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        })
    }
    
    func getTwitterEmail() {
        let client = TWTRAPIClient.withCurrentUser()
        view.showLoading()
        client.requestEmail { [weak self] email, error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            
            if let email = email {
                strongSelf.loginTwitter(email: email)
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func loginTwitter(email:String) {
        view.showLoading()
        service.loginTwitter(email) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                if user.username.isEmpty {
                    strongSelf.gotoLoginByTwitter(user: user)
                } else {
                    strongSelf.gotoHome()
                }
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }

    @IBAction func whyZakatifyBtnAction(_ sender: Any) {
        if let instance = OnboardingVC.instance() {
            self.navigationController?.pushViewController(instance, animated: true)
            ZMixPanelManager.shared.track(name: "Why Join Zakatify? Tapped", properties: [:])
        }
    }
    
}
