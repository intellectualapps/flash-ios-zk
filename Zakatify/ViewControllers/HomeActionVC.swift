//
//  HomeActionVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/8/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import RxSwift
enum HomeCellTag: String {
    case addPayment = "show-add-payment-screen"
    case showZakatGoal = "show-zakat-goal-screen"
    case showCharitiesFeed = "show-charity-feed-screen"
    case inviteFriend = "inviteFriend"
    case browserCurated = "browserCurated"
    case suggestCharities = "suggestCharities"
}

class HomeActionVC: BaseViewController, UserStatusView, UITableViewDelegate, UITableViewDataSource {
    var menuPresenter: SideMenuViewPresenter?
    var sections = [SectionInfo]()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    @IBAction func searchAction(_ sender: Any) {
        ZMixPanelManager.shared.track(name: "Search Tapped", properties: [:])
        if let navi = UIStoryboard.home().instantiateViewController(withIdentifier: "homeNavigation") as? UINavigationController {
            self.present(navi, animated: false, completion: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        menuPresenter = SideMenuPresenter(view: self)
        registerCell()
        prepare()
        menuPresenter?.refresh()
        searchBar?.placeholder = "Search charities"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        menuPresenter?.refresh()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ZMixPanelManager.shared.track(name: "Home Visited", properties: [:])
    }

    func fillData() {
        if let button = menuPresenter?.sideMenuModel.userStatus.button {
            if let model = menuPresenter?.sideMenuModel {
                UserManager.shared.sideMenuModel.value = model
            }
            if let last = sections.last {
                let cell = homeCellAction(tag: button.tag, text: button.text, iconName: "add_payment")
                if let first = last.cells.first {
                    if first.tag == HomeCellTag.addPayment.rawValue ||
                        first.tag == HomeCellTag.showZakatGoal.rawValue ||
                        first.tag == HomeCellTag.showCharitiesFeed.rawValue {
                        last.cells.removeFirst()
                    }
                }
                if button.tag.count > 0 {
                    last.cells.insert(cell, at: 0)
                }
                tableView?.reloadData()
            }

        }
    }

    func homeCellAction(tag: String, text: String, iconName: String) -> CellInfo {
        let cell = CellInfo(indentify: "HomeActionCell", aCellId: -1, height: 80, content: nil, title: text, cellColor: .clear)
        cell.iconName = iconName
        cell.tag = tag
        return cell
    }

    func registerCell() {
        searchBar?.backgroundColor = .clear
        searchBar?.backgroundImage = UIImage()
        let imgview = UIImageView(image: UIImage.init(named: "onboarding_bg"))
        imgview.contentMode = .scaleAspectFill
        imgview.frame = UIScreen.main.bounds
        tableView?.backgroundView = imgview
        self.view?.backgroundColor = .clear
        tableView?.backgroundColor = .clear
        tableView?.separatorStyle = .none
        var xib = UINib(nibName: "ProfileCell", bundle: nil)
        tableView?.register(xib, forCellReuseIdentifier: "ProfileCell")
        xib = UINib(nibName: "HomeBottomCell", bundle: nil)
        tableView?.register(xib, forCellReuseIdentifier: "HomeBottomCell")
        xib = UINib(nibName: "HomeActionCell", bundle: nil)
        tableView?.register(xib, forCellReuseIdentifier: "HomeActionCell")
        tableView?.allowsSelection = false
    }

    func prepare() {
        sections.removeAll()
        let sinfo = SectionInfo(indentify: "", title: "", height: 0.1, index: nil, contentObj: nil) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let cell = CellInfo(indentify: "ProfileCell", aCellId: -1, height: 193, content: nil, title: nil, cellColor: .clear)
            cells.append(cell)
            return cells
        }
        sections.append(sinfo)
        let sinfo2 = SectionInfo(indentify: "", title: "", height: 0.1, index: nil, contentObj: nil) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            var cell = homeCellAction(tag: HomeCellTag.inviteFriend.rawValue, text: "Invite friends & earn points", iconName: "invite_friend")
            cells.append(cell)

            cell = CellInfo(indentify: "HomeActionCell", aCellId: -1, height: 80, content: nil, title: "Browse curated funds", cellColor: .clear)
            cell.iconName = "browser"
            cell.tag = HomeCellTag.browserCurated.rawValue
            cells.append(cell)

            cell = CellInfo(indentify: "HomeActionCell", aCellId: -1, height: 80, content: nil, title: "Suggested charities for you", cellColor: .clear)
            cell.iconName = "suggest_charity"
            cell.tag = HomeCellTag.suggestCharities.rawValue
            cells.append(cell)


            cell = CellInfo(indentify: "HomeBottomCell", aCellId: -1, height: 98, content: nil, title: nil, cellColor: .clear)
            cells.append(cell)
            return cells
        }
        sections.append(sinfo2)
        tableView?.reloadData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sinfo = sections[indexPath.section]
        let row = sinfo.cells[indexPath.row]
        return row.height
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sinfo = sections[section]
        return sinfo.cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sinfo = sections[indexPath.section]
        let row = sinfo.cells[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: row.identifier, for: indexPath)
        (cell as? ProfileCell)?.fillData()
        if let actioncell = cell as? HomeActionCell {
            actioncell.config(cellInfo: row)
            actioncell.actionBtn?.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    if row.tag == HomeCellTag.inviteFriend.rawValue {
                        ZMixPanelManager.shared.track(name: "Invite Friends and Earn Points Tapped", properties: [:])
                    } else if row.tag == HomeCellTag.addPayment.rawValue {
                        ZMixPanelManager.shared.track(name: "Add a Payment Method Tapped", properties: [:])
                    }
                    self?.gotoScreen(tag: row.tag)
                }).disposed(by: actioncell.dispose)
        }

        if let actioncell = cell as? HomeBottomCell {
            actioncell.howToEarnBtn?.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let user = UserManager.shared.currentUser?.username else {
                        return
                    }
                    let title = "How do you earn/redeem points?"

                    let urlstr = "https://zakatify.com/app/poi/\(user)?key=eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzIwMTk0ODcsImlzcyI6InBodXMiLCJleHAiOjE1MzIxMDU4ODd9.-7H1PnnyPbSo1xNYQ7IAOci78PoZ0XONPIaxWAo22-E"
                    if let url = URL.init(string: urlstr) {
                        self?.openWebview(url: url, title: title)
                    }
                    ZMixPanelManager.shared.track(name: "How do You Earn/Redeem Points Tapped", properties: [:])
                }).disposed(by: actioncell.dispose)
            actioncell.curatedBtn?.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    let urlstr = "https://www.zakatify.com/app/funds"
                    let title = "What are curated portfolioes?"
                    if let url = URL.init(string: urlstr) {
                        self?.openWebview(url: url, title: title)
                    }
                    ZMixPanelManager.shared.track(name: "What are Curated Portfolios Tapped?", properties: [:])
                }).disposed(by: actioncell.dispose)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }

    func gotoScreen(tag: String) {
        if let hometag = HomeCellTag(rawValue: tag) {
            switch hometag {
            case .addPayment:
                showAddPayments()
            case .showZakatGoal:
                showZakaGoal()
            case .showCharitiesFeed:
                showCharitiesFeed()
            case .browserCurated:
                ZMixPanelManager.shared.track(name: "Browse Curated Portfolios Tapped", properties: [:])
                gotoCuratedListVC()
                break
            case .inviteFriend:
                showInvite()
                break
            case .suggestCharities:
                ZMixPanelManager.shared.track(name: "Suggested Charities For You Tapped", properties: [:])
                showCharitiesFeed()
            }
        }
    }

    func gotoCuratedListVC() {
        if let controller = CuratedListVC.newInstance() {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    func showCharitiesFeed() {
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            controller.isFirstTime = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    func showAddPayments() {
        if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "PaymentsViewController") as? PaymentsViewController {
            vc.mode = .edit
            vc.previousScreen = "Onboarding"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func showZakaGoal() {
        if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "PreferencesViewController") as? PreferencesViewController {
            vc.mode = .edit
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        ZMixPanelManager.shared.track(name: "Home Scrolled", properties: [:])
    }
}
