//
//  UserCuratedFundListVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/25/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class UserCuratedFundListVC: CuratedListVC {
    var allowsFetchData = false

    override func didRemoveCurated(info: CuratedInfo) {
        DispatchQueue.main.async {[weak self] in
            self?.viewModel.removeCurated(id: info.fundId)
            self?.tbView?.reloadData()
        }
    }

    func getDataForFirstTimeIfNeed() {
        allowsFetchData = true
        if viewModel.listCurated.value.count == 0 {
            getCuratedData(isReload: true)
        }
    }

    override func getCuratedData(isReload: Bool = true) {
        if !allowsFetchData {
            return
        }
        WindowManager.shared.showProgressView()
        viewModel.getOwnCuratedList(isReload: isReload) {[weak self] (response, error) in
            WindowManager.shared.hideProgressView()
            self?.stopAnimation()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }
        }
    }
}
