//
//  CuratedListVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CuratedListVC: BaseViewController {
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var tbView: UITableView!

    let viewModel = CuratedViewModel()

    class func newInstance() -> CuratedListVC? {
        let storyboard = UIStoryboard.init(name: "Curated", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CuratedListVC") as? CuratedListVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupGUI()
        viewModel.listCurated.asObservable()
            .subscribe(onNext: {[weak self] (listCurated) in
                DispatchQueue.main.async {
                    self?.tbView?.reloadData()
                    self?.tbView?.infiniteScrollingView?.enabled = listCurated.count > 0
                }
            }).disposed(by: rx.disposeBag)
        getCuratedData(isReload: true)

        tbView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.getCuratedData(isReload: true)
        })

        tbView?.addInfiniteScrolling(actionHandler: {[weak self] in
            self?.getCuratedData(isReload: false)
        })
        tbView?.infiniteScrollingView?.enabled = false
    }

    func didRemoveCurated(info: CuratedInfo) {

    }

    func didAddCurated(info: CuratedInfo) {
        let tags = info.categories.map({$0.description}).joined(separator: ",")
        ZMixPanelManager.shared.trackFundAdded(name: info.name, tags: tags, isCharity: false)
        DispatchQueue.main.async {[weak self] in
            self?.viewModel.removeCurated(id: info.fundId)
            self?.tbView?.reloadData()
        }
    }

    func getCuratedData(isReload: Bool = true) {
        WindowManager.shared.showProgressView()
        viewModel.getCuratedList(isReload: isReload) {[weak self] (response, error) in
            WindowManager.shared.hideProgressView()
            self?.stopAnimation()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }
        }
    }

    func stopAnimation() {
        DispatchQueue.main.async {[weak self] in
            self?.tbView?.pullToRefreshView?.stopAnimating()
            self?.tbView?.infiniteScrollingView?.stopAnimating()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Curated funds"
        addBackButtonDefault()

    }

    func setupGUI() {
        tbView?.backgroundColor = .clear
        tbView?.tableFooterView = UIView()
        tbView?.tableHeaderView = UIView()
        tbView?.delegate = self
        tbView?.dataSource = self
        tbView?.estimatedRowHeight = 100
        tbView?.rowHeight = UITableView.automaticDimension
        let xib = UINib(nibName: "CuratedFundCell", bundle: nil)
        tbView?.register(xib, forCellReuseIdentifier: "CuratedFundCell")
    }

    override func toggleLeft() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CuratedListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CuratedFundCell.identifier, for: indexPath) as? CuratedFundCell else {
            return UITableViewCell()
        }
        let model = viewModel.listCurated.value[indexPath.row]
        cell.curetedInfo = model
        cell.clickRemoveBlock = {[weak self] in
            WindowManager.shared.showProgressView()
            self?.viewModel.removeCuratedFund(fundid: model.fundId, completion: { (response, error) in
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else {
                    model.selected = false
                    cell.dataChanged()
                    self?.didRemoveCurated(info: model)
                }
            })
        }

        cell.clickAddBlock = {[weak self] in
            WindowManager.shared.showProgressView()
            self?.viewModel.addToPortfolio(fundid: model.fundId, completion: {[weak self] (response, error) in
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else {
                    model.selected = true
                    cell.dataChanged()
                    self?.didAddCurated(info: model)
                }
            })
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listCurated.value.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        tableView.deselectRow(at: indexPath, animated: false)
        let model = viewModel.listCurated.value[indexPath.row]
        let tags = model.categories.map({$0.description}).joined(separator: ",")
        ZMixPanelManager.shared.trackFundTapped(previousScreen: "Portfolio",
                                                name: model.name,
                                                tags: tags,
                                                categories: tags)
        if let controller = CuratedFundDetailVC.newInstance() {

            controller.viewModel.curatedInfo.value = model
            controller.viewModel.listViewModel = viewModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
