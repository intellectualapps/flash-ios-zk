//
//  HomeActionCell.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/9/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class HomeActionCell: UITableViewCell {
    var dispose = DisposeBag()
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        backView?.layer.cornerRadius = 20
        backView?.layer.masksToBounds = true
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        dispose = DisposeBag()
        clear()
    }

    func clear() {
        actionBtn?.setImage(nil, for: .normal)
        titleLbl?.text = nil
    }

    func config(cellInfo: CellInfo) {
        actionBtn?.setImage(UIImage.init(named: cellInfo.iconName), for: .normal)
        titleLbl?.text = cellInfo.title
    }
}
