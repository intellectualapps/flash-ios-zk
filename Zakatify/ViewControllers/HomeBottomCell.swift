//
//  HomeBottomCell.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/9/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import RxSwift

class HomeBottomCell: UITableViewCell {
    var dispose = DisposeBag()
    @IBOutlet weak var curatedBtn: UIButton!
    @IBOutlet weak var howToEarnBtn: UIButton!

    override func prepareForReuse() {
        super.prepareForReuse()
        dispose = DisposeBag()
    }
}
