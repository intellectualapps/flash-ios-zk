//
//  CuratedFundDetailTableVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CuratedFundDetailTableVC: UITableViewController {
    var didSetupObserver = false
    var viewModel: CuratedDetailViewModel? {
        didSet {
            if let _ = self.viewModel {
                self.setupObserver()
            }
        }
    }

    func setupObserver() {
        if didSetupObserver {
            return
        }
        if let md = viewModel {
            didSetupObserver = true
            md.curatedInfo.asObservable()
                .subscribe(onNext: {[weak self] (info) in
                    DispatchQueue.main.async {
                        self?.dataChanged()
                    }
                }).disposed(by: rx.disposeBag)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension

        /* Tạm ẩn đi chart review
         tableView.register(UINib(nibName: "ChartTableViewCell", bundle: nil), forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)
         */
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: ReviewTableViewCell.reuseIdentifier)
        let xib = UINib(nibName: "PortfolioCharityCell", bundle: nil)
        tableView?.register(xib, forCellReuseIdentifier: "PortfolioCharityCell")
    }


    func dataChanged() {
        self.tableView?.reloadData()
    }

    @IBAction func add(_ sender: Any) {

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
        /* Tạm ẩn đi chart review
         return 3
         */
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        } else if section == 1 {
            return viewModel?.curatedInfo.value?.charities.count ?? 0
        }
        else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        else {
            return 30
        }
    }

//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 1 {
//            return 70
//        }
//        return UITableView.automaticDimension
//    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = UIView.loadFromNibNamed(nibNamed: "TableViewSectionHeader") as? TableViewSectionHeader else {
            return nil
        }

        if section == 0 {
            return nil
        } else if section == 1 {
            view.lb_title?.text = "PORTFOLIO CHARITIES"
        }
        else {
            view.lb_title.text = "NO REVIEWS"
        }
        return view
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.clearBackgroundHeaderFooterView()
    }

    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.clearBackgroundHeaderFooterView()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CuratedDetailCell", for: indexPath) as? CuratedDetailCell else {
                return UITableViewCell()
            }

            // Configure the cell...
            cell.delegate = self
            cell.updateContent(viewModel: viewModel)
            return cell
        } else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PortfolioCharityCell", for: indexPath) as? PortfolioCharityCell else {
                return UITableViewCell()
            }
            if let info = viewModel?.curatedInfo.value?.charities[indexPath.row] {
                cell.config(charity: info)
            }
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.reuseIdentifier, for: indexPath) as? ReviewTableViewCell else {
                return UITableViewCell()
            }
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
            return
        }
        vc.previousScreen = "Fund*"
        if let info = viewModel?.curatedInfo.value?.charities[indexPath.row] {
            let params = RequestParams()
            params.setValue(info.name, forKey: "screen_name")
            UIViewController.logEvent(eventName: .openProfile, parameters: params)
            vc.charity = info
            self.navigationController?.pushViewController(vc, animated: true)
            ZMixPanelManager.shared.trackVisitProfile(lastScreen: "Charity")
        }
    }
}

extension CuratedFundDetailTableVC: CuratedDetailCellDelegate {
    func clickWebsite() {

    }

    func clickEmail() {

    }

    func clickDonate() {
        let count = (Defaults.numberOfDonateTap.getInt() ?? 0) + 1
        Defaults.numberOfDonateTap.set(value: count)
        let name = viewModel?.curatedInfo.value?.name ?? ""
        let properties: [String: MixpanelType] = [
            "Name": name,
            "Number of Donate Now Taps": count,
            "Donation Type": "Fund*",
            "Tags": viewModel?.curatedInfo.value?.slogan ?? ""]
        ZMixPanelManager.shared.track(name: "Donate Now Tapped", properties: properties, alsoSetPeopleProperties: true)
    }

    func clickAddPortfolio() {

        guard let model = self.viewModel else {
            return
        }
        guard let info = model.curatedInfo.value else {
            return
        }
        if info.selected {
            let name = viewModel?.curatedInfo.value?.name ?? ""
            ZMixPanelManager.shared.track(name: "Remove from Portfolio Tapped", properties: ["Name": name])
            WindowManager.shared.showProgressView()
            model.removeCuratedFund(fundid: model.curatedInfo.value?.fundId ?? -1, completion: {[weak self] (response, error) in
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else {
                    model.curatedInfo.value?.selected = false
                    self?.tableView?.reloadData()
                    model.listViewModel?.removeCurated(id: info.fundId)
                }
            })
        } else {
            let name = viewModel?.curatedInfo.value?.name ?? ""
            let cates = model.curatedInfo.value?.categories.map({$0.description}).joined(separator: ",") ?? ""
            let profile: [String: MixpanelType] = [
                "Name": name,
                "Tags": model.curatedInfo.value?.slogan ?? "",
                "Categories": cates,
                "charity or fund?": "fund"
            ]
            ZMixPanelManager.shared.track(name: "Add to Portfolio Tapped", properties: profile)
            WindowManager.shared.showProgressView()
            model.addToPortfolio(fundid: model.curatedInfo.value?.fundId ?? -1, completion: {[weak self] (response, error) in
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else {
                    model.curatedInfo.value?.selected = true
                    self?.tableView?.reloadData()
                    info.selected = true
                    model.listViewModel?.addCurated(info: info)
                }
            })
        }
    }
}
