//
//  DonationTableViewCell.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class DonationTableViewCell: UITableViewCell {
    static let reuseIdentifier: String = "DonationTableViewCell"
    @IBOutlet weak var charityLogoImageView: ImageView!
    @IBOutlet weak var charityNameLabel: UILabel!
    @IBOutlet weak var narrativeLabel: UILabel!
    
    var userDonationHistory: UserDonationHistoryInfo? {
        didSet {
            guard let userDonationHistory = self.userDonationHistory else {
                return
            }
            if let url = URL(string: userDonationHistory.charityLogoUrl) {
                charityLogoImageView.sd_setImage(with: url,
                                                 placeholderImage: UIImage(named: "noAvatar"))
            }
            charityNameLabel.text = userDonationHistory.charityName
            narrativeLabel.text = userDonationHistory.narrative
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
