//
//  CharityHeaderTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/7/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
protocol CuratedDetailCellDelegate: class {
    func clickDonate()
    func clickAddPortfolio()
    func clickWebsite()
    func clickEmail()
}

class CuratedDetailCell: UITableViewCell {
    @IBOutlet weak var uv_avatar_boder: UIView!
    @IBOutlet weak var iv_avatar: UIImageView!
    
    @IBOutlet weak var donatedByLbl: UILabel!
    @IBOutlet weak var lb_charityName: UILabel!
    @IBOutlet weak var lb_rate: UILabel!
    
    @IBOutlet weak var bt_add: Button!
    
    @IBOutlet weak var uv_donor: UIView!
    @IBOutlet weak var uv_donor_avatars: UIView!
    @IBOutlet weak var lb_donor_status: AutocConstraintLabel!
    @IBOutlet weak var collectionView_donor_avatar: UICollectionView!
    @IBOutlet weak var collectionViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lb_charityDescription: UILabel!

    @IBOutlet weak var categoryTagView: TagListView! {
        didSet {
            categoryTagView?.alignment = .left
            categoryTagView?.delegate = self
        }
    }

    @IBOutlet weak var tagView: TagListView! {
        didSet {
            tagView?.alignment = .left
            tagView?.delegate = self
        }
    }
    
    var viewModel: CuratedDetailViewModel?
    
    var bubblePictures: BubblePictures!
    
    weak var delegate: CuratedDetailCellDelegate?

    func updateContent(viewModel: CuratedDetailViewModel?) {
        self.viewModel = viewModel
         dataChanged()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uv_avatar_boder.addDashedBorder(color: UIColor.blueBorderColor, width: 3, space: 3)
        let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 38))
        iv.image = #imageLiteral(resourceName: "ic-dolar")
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func clickDonate(_ sender: Any) {
        delegate?.clickDonate()
    }

    @IBAction func clickAdd(_ sender: Any) {
        delegate?.clickAddPortfolio()
    }
    @IBAction func clickReport(_ sender: Any) {
//        guard let presenter = presenter, let url = URL(string:presenter.charity.reportUrl) else {
//            return
//        }
//        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func clickWebsite(_ sender: UIButton) {
        delegate?.clickWebsite()
    }
    
    @IBAction func clickEmail(_ sender: UIButton) {
        delegate?.clickEmail()
    }

    func clear() {
        lb_charityName?.text = nil
        lb_donor_status?.text = nil
        donatedByLbl?.text = nil
        lb_charityDescription?.text = nil
    }
}

extension CuratedDetailCell {
   
    func dataChanged() {
        guard let info = viewModel?.curatedInfo.value else {
            return
        }
        categoryFillData()
        lb_charityName.text = info.name
        lb_charityDescription.text = info.desc
        if let url = URL(string: info.logoUrl) {
            iv_avatar.sd_setImage(with: url)
        }
        let rateText = "☆☆☆☆☆"
        lb_rate.text = rateText
        
        if info.selected == true {
            bt_add.setTitle("Remove from Portfolio", for: UIControlState.normal)
        } else {
            bt_add.setTitle("Add to Portfolio", for: UIControlState.normal)
        }
        configAvatarCollectionView(info: info)
        lb_donor_status.text = "$\(info.totalDonation)"
        donatedByLbl?.text = "donated by \(info.numberOfDonators) Zakatifiers this year"

    }
    
    private func configAvatarCollectionView(info: CuratedInfo) {
        let configFiles = getConfigFiles(info: info)
        let count = min(configFiles.count, 5)
        collectionViewWidthConstraint?.constant = CGFloat(count) * 50
        let layoutConfigurator = BPLayoutConfigurator(backgroundColorForTruncatedBubble: UIColor.gray,
                                                      fontForBubbleTitles: UIFont(name: "HelveticaNeue-Light", size: 16.0)!,
                                                      colorForBubbleBorders: .white,
                                                      colorForBubbleTitles: .white,
                                                      maxCharactersForBubbleTitles: 5,
                                                      maxNumberOfBubbles: 5,
                                                      direction: .rightToLeft,
                                                      alignment: .center)
        
        bubblePictures = BubblePictures(collectionView: collectionView_donor_avatar,
                                        configFiles: configFiles,
                                        layoutConfigurator: layoutConfigurator)
    }
    
    private func getConfigFiles(info: CuratedInfo) -> [BPCellConfigFile] {
        var fileArr = [BPCellConfigFile]()
        for url in info.donorsAvatarUrls {
            let cell = BPCellConfigFile(imageType: BPImageType.URL(url), title: "")
            fileArr.append(cell)
        }
        if fileArr.isEmpty {
            let cell = BPCellConfigFile(imageType: BPImageType.URL(UserManager.shared.currentUser?.profileUrl ?? ""),
                                        title: "")
            fileArr.append(cell)
        }
        return fileArr
    }
}

// MARK: - CategoryView
extension CuratedDetailCell: CategoryView {
    func added() {
        
    }
    
    func categoryFillData() {
        guard let info = viewModel?.curatedInfo.value,
            let tagView = tagView else {
                return
        }
        tagView.removeAllTags()
        let tags = info.categories.map({$0.description})
        tagView.addTags(tags)

        categoryTagView?.removeAllTags()
        categoryTagView?.addTags(tags)
    }
}

extension CuratedDetailCell: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
    }
}
