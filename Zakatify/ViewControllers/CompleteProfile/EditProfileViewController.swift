//
//  EditProfileViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import IQKeyboardManagerSwift


@objc class EditProfileViewController: UITableViewController {
    var disposeBag = DisposeBag()
    let imagePicker = UIImagePickerController()
    var percentCompleted: Double = 0
    var pictureObserver: Observable<UIImagePickerController>?
    @IBOutlet weak var followerStack: UIStackView!
    @IBOutlet weak var iv_RateChart: CircleView!
    @IBOutlet weak var lb_rateValue: UILabel!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var bt_camera: UIButton!
    //
    @IBOutlet weak var lb_fullName: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var lb_portfolio: UILabel!
    @IBOutlet weak var lb_donations: UILabel!
    @IBOutlet weak var lb_followers: UILabel!
    @IBOutlet weak var lb_badges: UILabel!
    
    @IBOutlet weak var tf_fisrtname: UITextField!
    @IBOutlet weak var tf_lastname: UITextField!
    @IBOutlet weak var tf_mobile: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_location: UITextField!
    @IBOutlet weak var tf_facebookEmail: UITextField!
    @IBOutlet weak var bt_connectFacebook: Button!
    @IBOutlet weak var tf_twitterEmail: UITextField!
    @IBOutlet weak var bt_connectTwitter: Button!
    @IBOutlet weak var verifyImageView: UIImageView!
    
    // Category
    @IBOutlet weak var uv_Categories: TagListView! {
        didSet {
            uv_Categories.alignment = .center
            uv_Categories.delegate = self
        }
    }

    
    var presenter: UserInfoViewPresenter?
    var categoryPresenter: CategoryPresenter?

    weak var container: UIViewController?

    @objc func followerDidTap() {
        ZMixPanelManager.shared.track(name: "User's Followers Tapped", properties: [:])
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addBackButtonDefault()
        self.addSwipeRight()
        followerStack?.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(followerDidTap))
        followerStack?.addGestureRecognizer(tap)
        tf_mobile.keyboardType = .phonePad
        tf_fisrtname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userFirstName = self.tf_fisrtname.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_lastname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLastName = self.tf_lastname.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_mobile.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userMobile = self.tf_mobile.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_location.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLocation = self.tf_location.text ?? ""
            }.disposed(by: disposeBag)
        
        if let user = UserManager.shared.currentUser {
            presenter = UserInfoPresenter(view: self, model: user)
            categoryPresenter = CategoryPresenter(view: self)
            fillData()
        }
        categoryPresenter?.getAllCategory(completion: {[weak self] in
            self?.tableView?.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    @objc override func showLoading() {
        if let container = container {
            container.showLoading()
            return
        } else {
            super.showLoading()
        }
    }

    @objc override func hideLoading() {
        if let container = container {
            container.hideLoading()
            return
        } else {
            super.hideLoading()
        }
    }
}

// MARK: - CategoryView
extension EditProfileViewController: CategoryView {
    func added() {
        
    }
    
    func categoryFillData() {
        guard let presenter = categoryPresenter,
            let uv_Categories = uv_Categories else {
                return
        }
        uv_Categories.removeAllTags()
        let tags = presenter.categories.map { (category) in
            return category.description
        }
        uv_Categories.addTags(tags)
        let tagsView = uv_Categories.allTagsView()
        let selectedTags = presenter.userCategories.map { (category) in
            return category.description
        }
        let tagsViewNeedSelect = tagsView.filter { (tagview) -> Bool in
            if let title = tagview.currentTitle {
                return selectedTags.contains(title)
            }
            return false
        }
        
        DispatchQueue.main.async {
            for tagView in tagsViewNeedSelect {
                tagView.isSelected = true
            }
        }
    }
}

extension EditProfileViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        tagView.isSelected = !tagView.isSelected
    }
    

}

// MARK: - UserInfoView
extension EditProfileViewController: UserInfoView {
    func displayImage(img: UIImage?) {
        self.iv_avatar?.image = img
    }

    func fillData() {
        guard let presenter = self.presenter else {
            return
        }
        iv_RateChart.value = CGFloat(percentCompleted)
        let percentCompletedToInt = Int(percentCompleted * 100)
        lb_rateValue.text = "\(CGFloat(percentCompletedToInt) / 100)%"
        //
        lb_fullName.attributedText = presenter.user.nameAttributedString
        lb_username.text = "@" + presenter.username
        //
        if let editAvatar = presenter.editPhoto {
            self.displayImage(img: editAvatar)
        } else {
            if let url = presenter.photoUrl {
                iv_avatar.sd_setImage(with: url)
            }
        }
        
        tf_fisrtname.text = presenter.userFirstName
        tf_lastname.text = presenter.userLastName
        tf_email.text = presenter.userEmail
        tf_mobile.text = presenter.userMobile
        tf_location.text = presenter.userLocation
        
        tf_facebookEmail.text = presenter.userFacebookEmail
        bt_connectFacebook.setTitle(presenter.userFacebookEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
        
        
        tf_twitterEmail.text = presenter.userTwitterEmail
        bt_connectTwitter.setTitle(presenter.userTwitterEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
        
        lb_portfolio.text = String(presenter.user.portfolios)
        lb_followers.text = String(presenter.user.followers)
        lb_donations.text = String(presenter.user.donations)
        lb_badges.text = "None"
    }
    
    func saveSuccess() {
        let selectedTags = uv_Categories.selectedTags()
        let tagDescriptions = selectedTags.map { (tagview) in
            return tagview.currentTitle ?? ""
        }
        categoryPresenter?.add(categoryDescriptions: tagDescriptions)
    }
    
    @IBAction func choseImage(_ sender: Any) {
        ZMixPanelManager.shared.track(name: "User's Profile Picture Tapped", properties: [:])
        choseImageSource()
    }
    
    private func choseImageSource() {

        UIApplication.shared.keyWindow?.endEditing(true)
        let alert = UIAlertController(title: "", message: "Chose image from", preferredStyle: UIAlertController.Style.actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camera = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (action) in
                self.choseImageFromCamera()
            })
            alert.addAction(camera)
        }
        
        let library = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler: { (action) in
            self.choseImageFromPhotoLibrary()
        })
        alert.addAction(library)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
        }
        alert.addAction(cancel)
        self.present(alert, animated: true) {

        }
    }
    
    private func choseImageFromCamera() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func choseImageFromPhotoLibrary() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }

    func didConnectTwitter() {
        ZMixPanelManager.shared.track(name: "Twitter Connected", properties: ["Twitter Connected?": true])
        fillData()
    }

    func didConnectFacebook() {
        ZMixPanelManager.shared.track(name: "Facebook Connected", properties: ["Facebook Connected?": true])
        fillData()
    }
    
    @IBAction func connectTwitter(_ sender: Any) {
        if presenter?.userTwitterEmail.isEmpty == false {
            presenter?.userTwitterEmail = ""
            fillData()
             ZMixPanelManager.shared.track(name: "Twitter Disconnected", properties: ["Twitter Connected?": false])
            return
        }
        presenter?.loginTwitter()
    }
    
    @IBAction func connectFacebook(_ sender: Any) {
        if presenter?.userFacebookEmail.isEmpty == false {
            presenter?.userFacebookEmail = ""
            fillData()
            ZMixPanelManager.shared.track(name: "Facebook Disconnected", properties: ["Facebook Connected?": false])
            return
        }
        presenter?.loginFacebook()
    }
    
    @IBAction func next(_ sender: Any) {
        self.view.endEditing(true)
        presenter?.saveChange(completion: {_ in})
    }
}

// MARK: - UITableViewDelegate
extension EditProfileViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 161
            } else if indexPath.row == 1 {
                return 55
            } else if indexPath.row == 2 {
                return 70
            }
        } else if indexPath.section == 1 {
            return 50
        } else if indexPath.section == 2 {
            guard let presenter = categoryPresenter else {
                return 49
            }
            let countCategories = CGFloat(presenter.categories.count)
            /// Giả sử:
            /// - Mỗi dòng có 3 tagView
            /// - Mỗi tagView có chiều cao = 36,
            /// - Khoảng cách bottom của tagView trên với top của tagView dưới là 8
            /// - listTagViews.top = UITableViewCell.top + 49
            let countLineTagView = Int(countCategories / 3) + 1
            let countLineTagViewToCGFloat = CGFloat(countLineTagView)
            
            return 49.0 + countLineTagViewToCGFloat * 36.0 + (countLineTagViewToCGFloat-1) * 8
        }
        return 0
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            displayImage(img: pickedImage)
            presenter?.editPhoto = pickedImage
        }

        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)

    }
}
