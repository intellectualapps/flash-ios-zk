//
//  EditProfileContainerViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/17/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class EditProfileContainerViewController: UIViewController {
    
    var editProfileViewController:EditProfileViewController? {
        didSet {
            editProfileViewController?.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 20))
            editProfileViewController?.container = self
        }
    }
    var percentCompleted: Double = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addSwipeRight()
        self.addBackButtonDefault()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ZMixPanelManager.shared.track(name: "User's Profile Visited", properties: [:])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickSave(_ sender: Any) {
        if let firstname = self.editProfileViewController?.tf_fisrtname.text, firstname.count > 0 {
            let email = self.editProfileViewController?.tf_email?.text ?? ""
            let lastname = self.editProfileViewController?.tf_lastname?.text ?? ""
            let mobile = self.editProfileViewController?.tf_mobile?.text ?? ""
            let locaiton = self.editProfileViewController?.tf_location?.text ?? ""
            let twitterConnected = self.editProfileViewController?.bt_connectTwitter?.isSelected ?? false
            let facebookConnected = self.editProfileViewController?.bt_connectFacebook?.isSelected ?? false

            var charities = ""
            let user = self.editProfileViewController?.presenter?.user
            if let tags = self.editProfileViewController?.presenter?.userSelectedTags {
                charities = tags.map({$0.description}).joined(separator: ",")
            }
            let profile: [String: MixpanelType] =
                ["First Name": firstname,
                 "Last Name": lastname,
                 "Mobile": mobile,
                 "Email": email,
                 "Location": locaiton,
                 "Twitter Connected?": twitterConnected,
                 "Facebook Connected?": facebookConnected,
                 "Charity Interests": charities
            ]
            ZMixPanelManager.shared.track(name: "Profile Completed",
                                          properties: profile ,
                                          alsoSetPeopleProperties: true)
            ZMixPanelManager.shared.track(name: "Profile Edited", properties: profile)
            editProfileViewController?.next(sender)
        }
    }

    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let editProfileViewController = segue.destination as? EditProfileViewController {
            editProfileViewController.percentCompleted = percentCompleted
            self.editProfileViewController = editProfileViewController
        }
    }
    

}

