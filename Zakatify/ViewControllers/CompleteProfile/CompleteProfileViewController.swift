//
//  CompleteProfileViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/29/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CompleteProfileViewController: UITableViewController, UserInfoView {

    let imagePicker = UIImagePickerController()
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var iv_border: UIImageView!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var bt_camera: UIButton!

    @IBOutlet weak var tf_fisrtname: UITextField!
    @IBOutlet weak var tf_lastname: UITextField!
    @IBOutlet weak var tf_username: UITextField!
    @IBOutlet weak var tf_mobile: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_location: UITextField!
    @IBOutlet weak var tf_facebookEmail: UITextField!
    @IBOutlet weak var bt_connectFacebook: Button!
    @IBOutlet weak var tf_twitterEmail: UITextField!
    @IBOutlet weak var bt_connectTwitter: Button!
    
    var presenter: UserInfoViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.delaysContentTouches = true
        addBackButtonDefault()
        self.addSwipeRight()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        iv_border.addDashedBorder(color: UIColor.blueBorderColor, width: 3, space: 3)
        
        tf_fisrtname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userFirstName = self.tf_fisrtname.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_lastname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLastName = self.tf_lastname.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_mobile.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userMobile = self.tf_mobile.text ?? ""
            }.disposed(by: disposeBag)
        
        tf_location.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLocation = self.tf_location.text ?? ""
            }.disposed(by: disposeBag)
        
        if let user = UserManager.shared.currentUser {
            presenter = UserInfoPresenter(view: self, model: user)
            fillData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func didConnectTwitter() {
        ZMixPanelManager.shared.track(name: "Twitter Connected", properties: ["Twitter Connected?": true])
        fillData()
    }

    func didConnectFacebook() {
        ZMixPanelManager.shared.track(name: "Facebook Connected", properties: ["Facebook Connected?": true])
        fillData()
    }

    func displayImage(image: UIImage?) {
        self.iv_avatar?.image = image
    }

    func fillData() {
        guard let presenter = self.presenter else {
            return
        }
        if let editAvatar = presenter.editPhoto {
            self.displayImage(image: editAvatar)
        } else {
            if let url = presenter.photoUrl {
                iv_avatar?.sd_setImage(with: url, placeholderImage: UIImage(named: "noAvatar"))
            }
        }
        
        tf_fisrtname?.text = presenter.userFirstName
        tf_lastname?.text = presenter.userLastName
        tf_username?.text = presenter.username
        tf_email?.text = presenter.userEmail
        tf_mobile?.text = presenter.userMobile
        tf_location?.text = presenter.userLocation
        
        tf_facebookEmail?.text = presenter.userFacebookEmail
        bt_connectFacebook?.setTitle(presenter.userFacebookEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
        
        
        tf_twitterEmail?.text = presenter.userTwitterEmail
        bt_connectTwitter?.setTitle(presenter.userTwitterEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
    }
    
    @IBAction func choseImage(_ sender: Any) {
        choseImageSource()
    }
    
    private func choseImageSource() {
        let alert = UIAlertController(title: "", message: "Chose image from", preferredStyle: UIAlertController.Style.actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camera = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (action) in
                self.choseImageFromCamera()
            })
            alert.addAction(camera)
        }
        
        let library = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler: { (action) in
            self.choseImageFromPhotoLibrary()
        })
        alert.addAction(library)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
        }
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func choseImageFromCamera() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func choseImageFromPhotoLibrary() {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }

    @objc override func toggleLeft() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func connectTwitter(_ sender: Any) {
        if presenter?.userTwitterEmail.isEmpty == false {
            presenter?.userTwitterEmail = ""
            fillData()
            return
        }
        presenter?.loginTwitter()
    }
    
    @IBAction func connectFacebook(_ sender: Any) {
        if presenter?.userFacebookEmail.isEmpty == false {
            presenter?.userFacebookEmail = ""
            fillData()
            return
        }
        presenter?.loginFacebook()
    }
    
    @IBAction func next(_ sender: Any) {
        self.view.endEditing(true)
        presenter?.saveChange(completion: {[weak self] success in
            if let firstname = self?.tf_fisrtname?.text, firstname.count > 0 {
                let lastname = self?.tf_lastname?.text ?? ""
                let mobile = self?.tf_mobile?.text ?? ""
                let locaiton = self?.tf_location?.text ?? ""
                let twitterTitle = self?.bt_connectTwitter?.title(for: .normal) ?? ""
                let facebookTitle = self?.bt_connectFacebook?.title(for: .normal) ?? ""
                let twitterConnected = twitterTitle.lowercased() == "connect"
                let facebookConnected = facebookTitle.lowercased() == "connect"
                let profile: [String: MixpanelType] = ["First Name": firstname,
                               "Last Name": lastname,
                               "Mobile": mobile,
                               "Location": locaiton,
                               "Twitter Connected?": twitterConnected,
                               "Facebook Connected?": facebookConnected]
                ZMixPanelManager.shared.track(name: "Profile Completed",
                                              properties: profile,
                                              alsoSetPeopleProperties: true)
            }
        })
    }
    
    func saveSuccess() {
        self.performSegue(withIdentifier: "preference", sender: nil)
    }
    
    //
    
    //
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let footer = tableView.tableFooterView , let window = AppDelegate.shareInstance().window else {
            return
        }
        let windowFrame = window.frame
        let frameInWindow = tableView.convert(footer.frame, to: window)
        var height = windowFrame.size.height - frameInWindow.origin.y
        if height <= 60 {
            height = 60
        }
        footer.frame.size.height = height
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let footer = tableView.tableFooterView , let window = AppDelegate.shareInstance().window else {
            return
        }
        let windowFrame = window.frame
        let frameInWindow = tableView.convert(footer.frame, to: window)
        var height = windowFrame.size.height - frameInWindow.origin.y
        if height <= 60 {
            height = 60
        }
        footer.frame.size.height = height
    }

}
extension CompleteProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            displayImage(image: pickedImage)
            presenter?.editPhoto = pickedImage
        }

        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)

    }
}
