//
//  ReadMoreDonationsFooterView.swift
//  Zakatify
//
//  Created by MTQ on 9/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class ReadMoreDonationsFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var readMoreButton: UIButton!
}
