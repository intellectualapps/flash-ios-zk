//
//  MessageCollectionViewCellInComming.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 10/6/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class MessageCollectionViewCell: JSQMessagesCollectionViewCell {
    static let reuseIdentifier: String = "MessageCollectionViewCell"

    @IBOutlet weak var lb_date: JSQMessagesLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
