//
//  CharityDetailTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import SDWebImage

class CuratedFundCell: UITableViewCell {
    static let identifier: String = "CuratedFundCell"
    
    @IBOutlet weak var avatarBgView: UIView!
    @IBOutlet weak var lb_newfeed: UILabel!
    @IBOutlet weak var iv_logo: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_description: UILabel!
    @IBOutlet weak var v_donorAvatars: UIView!
    @IBOutlet weak var lb_donors: UILabel!
    @IBOutlet weak var tagView: TagListView! {
        didSet {
            tagView.alignment = .left
            tagView.delegate = self
        }
    }
    @IBOutlet weak var widthDonorAvatarsConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingDonorAvatarsConstraint: NSLayoutConstraint!
    
    lazy var bt_add: UIButton  = {
        let bt = UIButton(type: UIButton.ButtonType.custom)
        bt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        bt.setImage(#imageLiteral(resourceName: "ic-add"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.addPortfolio), for: UIControlEvents.touchUpInside)
        return bt
    }()
    lazy var bt_remove: UIButton = {
        let bt = UIButton(type: UIButton.ButtonType.custom)
        bt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        bt.setImage(#imageLiteral(resourceName: "ic-check"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.removePortfolio), for: UIControlEvents.touchUpInside)
        return bt
    }()
    
    var curetedInfo: CuratedInfo? {
        didSet {
            dataChanged()
        }
    }
    
    var clickAddBlock:(()->Void)?
    
    var clickRemoveBlock:(()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iv_logo.backgroundColor = UIColor.groupTableViewBackground
        avatarBgView?.addDashedBorder(color: UIColor(hex: 0x2a7092), width: 1, space: 1)
        avatarBgView?.round(radius: 29)
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        curetedInfo = nil
        lb_newfeed.attributedText = nil
        lb_name.text = nil
        lb_description.text = nil
        lb_donors.text = nil
    }
}

extension CuratedFundCell: CharityDetailView {
    var refTableView: UITableView? {
        return nil
    }

    var refPresenter: TableViewPresenter? {
        return nil
    }

    func userDidAddToPorfolio() {
        
    }
    func userDidDonate() {
        
    }
    func userDidWriteReview() {
        
    }
    func dataChanged() {
        guard let info = curetedInfo else {
            return
        }
        lb_newfeed.attributedText = info.newfeeds.newFeedAttributes()
        lb_name.text = info.name
        lb_description.text = info.slogan
        if let url = URL(string: info.logoUrl) {
            iv_logo.sd_setImage(with: url,
                                placeholderImage: UIImage(named: "noAvatar"))
        }
        var donorsString = info.donateDescription()
        if info.numberOfDonators == 0 {
            donorsString = "No donations yet \nBe the first to donate"
        }
        lb_donors.text = donorsString
        categoryFillData()
        var count = 0
        let width = v_donorAvatars.frame.size.height
        for iv in v_donorAvatars.subviews {
            iv.removeFromSuperview()
        }
        /* 18 = 90 /5
         90: v_donorAvatars.frame.size.width
         5: hiển thị max là 5
         */
        let rate: CGFloat = 18.0
        var counter = 0
        for url in info.donorsAvatarUrls {
            if counter >= 5 {
                break
            }
            counter += 1
            let x = CGFloat(count) * rate
            let iv = UIImageView(frame: CGRect(x: x, y: 0, width: width, height: width))
            iv.layer.masksToBounds = true
            iv.layer.cornerRadius = width/2.0
            iv.layer.borderWidth = 1.5
            iv.layer.borderColor = UIColor.white.cgColor
            if let url = URL(string: url) {
                iv.sd_setImage(with: url)
            } else {
                iv.image = UIImage(named: "noAvatar")
            }
            count += 1
            v_donorAvatars.addSubview(iv)
            v_donorAvatars.sendSubviewToBack(iv)
        }
        if counter == 0 {
            trailingDonorAvatarsConstraint.constant = 0
            widthDonorAvatarsConstraint.constant = 0
        } else {
            trailingDonorAvatarsConstraint.constant = 8
            widthDonorAvatarsConstraint.constant = width * CGFloat(count) - (width - rate) * CGFloat((count - 1))
        }
        
        if info.selected {
            self.accessoryView = bt_remove
        } else {
            self.accessoryView = bt_add
        }
    }
    
    @objc func addPortfolio() {
        self.clickAddBlock?()
    }
    
    @objc func removePortfolio() {
        self.clickRemoveBlock?()
    }
}

// MARK: - CategoryView
extension CuratedFundCell: CategoryView {
    func added() {
        
    }
    
    func categoryFillData() {
        guard let info = curetedInfo,
            let tagView = tagView else {
                return
        }
        tagView.removeAllTags()
        let tags = info.categories.map({$0.description})
        tagView.addTags(tags)
    }
}

extension CuratedFundCell: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
    }
}
