//
//  ProfileCell.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/8/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class ProfileCell: UITableViewCell {
    var dispose = DisposeBag()
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var iv_RateChart: CircleView!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_rateValue: UILabel!
    //
    @IBOutlet weak var lb_fullName: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    //
    @IBOutlet weak var lb_moneyDescription: UILabel!
    //
    @IBOutlet weak var lb_rankDescription: UILabel!
    @IBOutlet weak var goalProgressLbl: UILabel!

    @IBOutlet weak var rankMonthLbl: UILabel!
    @IBOutlet weak var pointMonthlbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView?.layer.cornerRadius = 30
        roundView?.layer.masksToBounds = true
        roundView?.alpha = 0.8
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        lb_rateValue?.text = nil
        lb_fullName?.text = nil
        lb_username?.text = nil
        lb_moneyDescription?.text = nil
        lb_rankDescription?.text = nil
        pointMonthlbl?.text = "0"
        goalProgressLbl?.text = "0%"
        rankMonthLbl?.text = "0"
    }

    func fillData() {
        //
        guard let model = UserManager.shared.sideMenuModel.value else {
            return
        }
        let status = model.userStatus
        if let url = model.user.avartarURL {
            iv_avatar.sd_setImage(with: url,
                                  placeholderImage: UIImage(named: "noAvatar"))
        }
        iv_RateChart?.value = CGFloat(status.donationProgress.percentCompleted)
        lb_rateValue?.text = "\(Float(status.donationProgress.percentCompleted))%"
        //
        let mutaAttr = model.user.nameAttributedString
        mutaAttr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], range: NSMakeRange(0, mutaAttr.length))
        lb_fullName?.attributedText = mutaAttr
        lb_username?.text = "@" + model.user.username
        //
        let moneyDescription = NSMutableAttributedString()
        let currentMoney = NSAttributedString(string: "$\(Int(status.donationProgress.totalDonation))",
            attributes: [NSAttributedString.Key.foregroundColor:UIColor.green,
                         NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        moneyDescription.append(currentMoney)

        let totalMoney = NSAttributedString(string: " out of $\(Int(status.donationProgress.goal))",
            attributes: [NSAttributedString.Key.foregroundColor:UIColor.green,
                         NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        moneyDescription.append(totalMoney)

        lb_moneyDescription?.attributedText = moneyDescription
        //
        lb_rankDescription?.attributedText = status.userBoard.descriptionAttributedString
        pointMonthlbl?.text = String(status.userBoard.monthPoints)
        goalProgressLbl?.text = "\(Float(status.donationProgress.percentCompleted))%"
        rankMonthLbl?.text = String(status.userBoard.monthRank)
    }
}
