//
//  OnboardingSubView.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/5/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class OnboardingSubView: UIView {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var extraLbl: UILabel!
    
    class func instance() -> OnboardingSubView {
        return OnboardingSubView.fromNib(nibNameOrNil: "OnboardingSubView")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    func clear() {
        titleLbl?.text = nil
        iconBtn?.setImage(nil, for: .normal)
        extraLbl?.text = nil
    }

    func config(title: String, image: String, extra: String) {
        titleLbl?.text = title
        iconBtn?.setImage(UIImage.init(named: image), for: .normal)
        extraLbl?.text = extra
    }
}
