//
//  ToolTipView.swift
//  Zakatify
//
//  Created by center12 on 6/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ToolTipView: UIView {
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var image: UIImageView!
}
