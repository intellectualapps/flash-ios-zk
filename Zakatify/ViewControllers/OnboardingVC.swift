//
//  OnboardingVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/5/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import iCarousel

struct OnboardInfo {
    var title = ""
    var image = ""
    var extra = ""
}

class OnboardingVC: BaseViewController {
    @IBOutlet weak var icarousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    var dataSource = [OnboardInfo]()

    class func instance() -> OnboardingVC? {
        let storyboard = UIStoryboard.init(name: "Onboarding", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "OnboardingVC") as? OnboardingVC {
            return controller
        }
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.append(OnboardInfo(title: "DISCOVER 1,500+ ZAKAT-ELIGIBLE CHARITIES", image: "onboard3", extra: "Donate to vetted charities and a wide variety of causes"))
        dataSource.append(OnboardInfo(title: "REACH YOUR ANNUAL GIVING GOALS", image: "onboard1", extra: "Set your goal and we’ll provide personalized recommendations to help you reach it"))
        dataSource.append(OnboardInfo(title: "BUILD YOUR ZAKAT PORTFOLIO", image: "onboard2", extra: "Add charities to your portfolio and give to causes that matter to you"))
        dataSource.append(OnboardInfo(title: "DONATE ON YOUR OWN TERMS", image: "onboard5", extra: "Make automatic monthly donations and/or donate manually any time"))
        icarousel?.delegate = self
        icarousel?.dataSource = self
        icarousel?.type = .rotary
        icarousel?.bounceDistance = 10
        icarousel?.isPagingEnabled = true
    }

    @IBAction func startAction(_ sender: Any) {
        AppDelegate.shared.showRegisterScreen()
        var properties = ["Previous Screen": "Discover 1500+ Zakat-Eligible Charities"]
        if (self.icarousel.currentItemIndex == 1) {
            properties["Previous Screen"] = "Reach Your Annual Zakat Goal"
        } else if (self.icarousel.currentItemIndex == 2) {
            properties["Previous Screen"] = "Build Your Zakat Portfolio"
        } else {
            properties["Previous Screen"] = "Donate on Your Own Terms"
        }
        ZMixPanelManager.shared.track(name: "Onboarding Skipped",
                                      properties: properties,
                                      alsoSetPeopleProperties: true)
    }
}

extension OnboardingVC: iCarouselDataSource, iCarouselDelegate {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 4
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let aview = OnboardingSubView.instance()
        if index < dataSource.count {
            let info = dataSource[index]
            aview.config(title: info.title, image: info.image, extra: info.extra)
        }
        return aview
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return 0.6 * self.view.bounds.width
    }

    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        print("current index = \(carousel.currentItemIndex)")
        pageControl?.currentPage = carousel.currentItemIndex
    }

    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {

        return value
    }
}
