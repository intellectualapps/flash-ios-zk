//
//  BaseTableViewController.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 3/21/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import UIKit


class BaseTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("VIEWCONTROLLER \(NSStringFromClass(self.classForCoder))")
    }
}
