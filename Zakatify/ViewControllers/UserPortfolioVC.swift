//
//  UserPortfolioVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/25/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class UserPortfolioVC: BaseViewController {
    
    @IBOutlet weak var charitieContainer: UIView!
    @IBOutlet weak var curatedFundContainer: UIView!
    @IBOutlet weak var charitiesBtn: UIButton!
    @IBOutlet weak var curatedFundBtn: UIButton!
    @IBOutlet weak var lineView: UIView!
    var selectedBtn: UIButton?
    
    class func newInstance() -> UserPortfolioVC? {
        let storyboard = UIStoryboard.init(name: "UserPortfolio", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "UserPortfolioVC") as? UserPortfolioVC 
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedBtn = charitiesBtn
        self.title = "User Portfolio"
        addBackButtonDefault()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectBtn(btn: selectedBtn)
        ZMixPanelManager.shared.track(name: "Portfolio Visited", properties: [:])
    }

    func selectBtn(btn: UIButton?) {
        lineView?.frame.origin.x = btn?.frame.origin.x ?? 0
        charitiesBtn?.titleLabel?.textColor = UIColor.init(hex: 0x88847B)
        curatedFundBtn?.titleLabel?.textColor = UIColor.init(hex: 0x88847B)
        btn?.titleLabel?.textColor = UIColor.darkText
        if btn == charitiesBtn {
            charitieContainer?.isHidden = false
            curatedFundContainer?.isHidden = true
        } else {
            charitieContainer?.isHidden = true
            curatedFundContainer?.isHidden = false
        }
    }

    @IBAction func selectCharitiesAction(_ sender: Any) {
        self.selectedBtn = sender as? UIButton
        selectBtn(btn: selectedBtn)
    }

    @IBAction func selectCuratedAction(_ sender: Any) {
        self.selectedBtn = sender as? UIButton
        selectBtn(btn: selectedBtn)
        userCuratedFundVC()?.getDataForFirstTimeIfNeed()
    }

    func userCuratedFundVC() -> UserCuratedFundListVC? {
        for child in children {
            if let controller = child as? UserCuratedFundListVC {
                return controller
            }
        }
        return nil
    }
}
