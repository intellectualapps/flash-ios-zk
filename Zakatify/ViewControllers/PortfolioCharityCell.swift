//
//  PortfolioCharityCell.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import SDWebImage

class PortfolioCharityCell: UITableViewCell {

    @IBOutlet weak var avatar: ImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var donationStatusLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        avatar?.image = UIImage.init(named: "noAvatar")
        nameLbl?.text = nil
        donationStatusLbl?.text = nil
    }

    func config(charity: Charity) {
        let placeholder = UIImage.init(named: "noAvatar")
        if charity.logoUrl.count > 0 {
            avatar?.sd_setImage(with: URL.init(string: charity.logoUrl), placeholderImage: placeholder, options: SDWebImageOptions.cacheMemoryOnly, completed: {[weak self] (image, error, type, url) in
                if let img = image {
                    self?.avatar?.image = img
                }
            })
        }
        nameLbl?.text = charity.name
        var status = "NO donation yet"
        if charity.numberOfDonators > 0 {
            status = "$\(charity.totalMoney) donated by \(charity.numberOfDonators) zakatifiers"
        }
        donationStatusLbl?.text = status
    }
}
