//
//  CuratedFundDetailVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/24/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CuratedFundDetailVC: BaseViewController {
    @IBOutlet weak var bt_review: UIButton!
    var detailController: CuratedFundDetailTableVC? {
        get {
            let controller = children.filter({$0.isKind(of: CuratedFundDetailTableVC.self)}).first
            return controller as? CuratedFundDetailTableVC
        }
    }
    let viewModel = CuratedDetailViewModel()

    class func newInstance() -> CuratedFundDetailVC? {
        let storyboard = UIStoryboard.init(name: "Curated", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CuratedFundDetailVC") as? CuratedFundDetailVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Curated fund details"
        // Do any additional setup after loading the view.
        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
            self.addSwipeRight()
        }

        viewModel.curatedInfo.asObservable()
            .subscribe(onNext: {[weak self] (info) in
                DispatchQueue.main.async {
                    self?.dataChanged()
                }
            }).disposed(by: rx.disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WindowManager.shared.showProgressView()
        viewModel.refresh { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }
        }
        detailController?.viewModel = viewModel
    }

    @IBAction func review(_ sender: Any) {

    }

    func dataChanged() {
        let bt_title = "Write the first review"
        bt_review.setTitle(bt_title, for: UIControlState.normal)
        self.detailController?.tableView?.reloadData()
        self.detailController?.tableView?.tableFooterView = nil
    }
}
