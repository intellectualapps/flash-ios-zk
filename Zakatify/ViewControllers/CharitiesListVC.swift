//
//  CharitiesListVC.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/25/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class CharitiesListVC: HomeViewController {
    override func getCharities() {
        refreshControlChange()
    }

    override func loadMore() {
        loadData(isReload: false)
    }

    override func refreshControlChange() {
        loadData(isReload: true)
    }

    func loadData(isReload: Bool = true) {
        if let pr = presenter as? CharityListPresenter {
            self.showLoading()
            pr.getOwnCharities(isReload: isReload) {[weak self] (response, error) in
                self?.hideLoading()
            }
        }
    }
}
