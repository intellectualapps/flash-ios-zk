//
//  Page.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/13/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
protocol Page {
    var page: Int {get set}
    var size: Int {get set}
    var total: Int {get set}
}

struct PageCount: Page {
    var page: Int = 1
    var size: Int = 50
    var total: Int = 0
}
