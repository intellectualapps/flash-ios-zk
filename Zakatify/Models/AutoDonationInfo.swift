//
//  AutoDonationInfo.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 3/21/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class AutoDonationInfo: NSObject, NSCoding {
    var userOptsInAutoDonation = false
    var connectedPaypal = false
    var setDonationGoal = false
    var addCharityOrCurated = false

    func isReady() -> Bool {
        return userOptsInAutoDonation && connectedPaypal && setDonationGoal && addCharityOrCurated
    }

    override init() {
        super.init()
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(userOptsInAutoDonation, forKey: "userOptsInAutoDonation")
        aCoder.encode(connectedPaypal, forKey: "connectedPaypal")
        aCoder.encode(setDonationGoal, forKey: "setDonationGoal")
        aCoder.encode(addCharityOrCurated, forKey: "addCharityOrCurated")
    }

    required init?(coder aDecoder: NSCoder) {
        userOptsInAutoDonation = aDecoder.decodeBool(forKey: "userOptsInAutoDonation")
        connectedPaypal = aDecoder.decodeBool(forKey: "connectedPaypal")
        setDonationGoal = aDecoder.decodeBool(forKey: "setDonationGoal")
        addCharityOrCurated = aDecoder.decodeBool(forKey: "addCharityOrCurated")
    }

    func save() {
        let path = String.documentDirectoryPath() + "/data.dat"
        print(path)
        let data = NSKeyedArchiver.archivedData(withRootObject: self)

        FileManager.default.createFile(atPath: path, contents: data, attributes: [:])
    }

    class func loadDonationInfo() -> AutoDonationInfo {
        let path = String.documentDirectoryPath() + "/data.dat"
        if let data = try? Data.init(contentsOf: URL.init(fileURLWithPath: path)) {
            if let info = NSKeyedUnarchiver.unarchiveObject(with: data) as? AutoDonationInfo {
                return info
            }
        }
        return AutoDonationInfo()
    }
}
