//
//  CuratedInfo.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class CuratedInfo: Mappable {
    var fundId = 0
    var newfeeds: [Feed] = []
    var name = ""
    var desc = ""
    var slogan = ""
    var logoUrl = ""
    var donors: [Donor] = []
    var numberOfDonators = 0
    var selected = false
    var totalDonation: Double = 0
    var charities = [Charity]()
    var categories = [Category]()

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        fundId <- map["fundId"]
        newfeeds <- map["newsFeed"]
        name <- map["name"]
        desc <- map["description"]
        slogan <- map["slogan"]
        logoUrl <- map["logoUrl"]
        donors <- map["donors"]
        numberOfDonators <- map["numberOfDonors"]
        selected <- map["selected"]
        totalDonation <- map["totalDonation"]
        charities <- map["charities"]
        categories <- map["categories"]
    }

    func donateDescription() -> String {
        if donors.count == 0 {
            return "No donations yet \nBe the first to donate"
        }
        return "\(numberOfDonators) donated $\(totalDonation)"
    }

    var donorsAvatarUrls:[String] {
        var urls = [String]()
        for donor in donors {
            urls.append(donor.profileUrl)
        }
        return urls
    }
}
