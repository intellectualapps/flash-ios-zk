//
//  Payment.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class Payment: Mappable {
    var tranform: DateFormatterTransform {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let tranform = DateFormatterTransform(dateFormatter: dateFormatter)
        return tranform
    }
    enum type: String {
        case credit = "CC"
        case paypal = "PP"
    }
    
    enum JSONKey: String {
        case id = "payOptionId"
        case createDate
        case isPrimary
        case lastFourDigit
        case paymentType
        case token
        case username
        case email
    }
    var id: String = ""
    var createDate: Date?
    var isPrimary: Int = 0
    var lastFourDigit: Int = 0
    var paymentType: type = .paypal
    var token: String = ""
    var username: String = ""
    var email: String = ""
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        username <- map[JSONKey.username.rawValue]
        email <- map[JSONKey.email.rawValue]
        token <- map[JSONKey.token.rawValue]
        isPrimary <- map[JSONKey.isPrimary.rawValue]
        lastFourDigit <- map[JSONKey.lastFourDigit.rawValue]
        paymentType <- map[JSONKey.paymentType.rawValue]
        createDate = tranform.transformFromJSON(map[JSONKey.createDate.rawValue])
    }
    
    func toJSON() -> [String : Any] {
        let json = [JSONKey.username.rawValue:username,
                    JSONKey.token.rawValue:token,
                    "payment-type":paymentType.rawValue,
                    "last-four":lastFourDigit,
                    "is-primary":isPrimary] as [String : Any]
        return json
    }
}
