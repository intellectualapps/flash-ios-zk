//
//  PaypalAccount.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

struct PaypalAccount {
    let dateCreated: String
    let id: String
    let email: String
    let firstName: String
    let lastName: String
    let status: String

    func fullname() -> String {
        return firstName + " " + lastName
    }

    init(dict: [String: Any]) {
        id = dict.stringOrEmptyForKey(key: "id")
        dateCreated = dict.stringOrEmptyForKey(key: "dateCreated")
        email = dict.stringOrEmptyForKey(key: "paypalEmail")
        firstName = dict.stringOrEmptyForKey(key: "paypalFirstName")
        lastName = dict.stringOrEmptyForKey(key: "paypalLastName")
        status = dict.stringOrEmptyForKey(key: "status")
    }

    func paymentInfo() -> Payment {
        let info = Payment()
        info.id = id
        //"2018-04-18T00:00:00",
        info.createDate = dateCreated.getDateUTCFromString(format: "yyyy-MM-ddTHH:mm:ss")
        info.isPrimary = status.uppercased() == "PRIMARY" ? 1 : 0
        info.paymentType = .paypal
        info.username = fullname()
        info.email = email
        return info
    }
}
