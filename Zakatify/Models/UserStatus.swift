//
//  UserStatus.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/9/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

struct UserStatus {
    var donationProgress = UserDonationProgess()
    var userBoard = UserBoard()
    var notification = Count()
    var message = Count()
    var userCount = UserCount()
    var button = ButtonAction(tag: "", text: "")

    static func new() -> UserStatus {
        return UserStatus(donationProgress: UserDonationProgess(),
                          userBoard: UserBoard(),
                          notification: Count(),
                          message: Count(),
                          userCount: UserCount(),
                          button: ButtonAction(tag: "", text: ""))
    }
}
