//
//  Zakat.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper


protocol Goal {
    var goal: Float {get set}
    var progress: Float {get set}
}

class Zakat: Mappable {
    var tranform: DateFormatterTransform {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let tranform = DateFormatterTransform(dateFormatter: dateFormatter)
        return tranform
    }
    enum JSONKey: String {
        case amount
        case frequencyId
        case period
        case ce = "cash"
        case gs = "gold"
        case re = "realEstate"
        case i = "investment"
        case pu = "personalUse"
        case l = "liability"
        case username
    }
    enum frequency:Int {
        case auto = 1
        case manual = 2
    }
    var amount: Float = 0
    var frequencyId: Int = 1
    var period: Date? = Date()
    //
    var ce: Float = 0
    var gs: Float = 0
    var re: Float = 0
    var i: Float = 0
    //
    var pu: Float = 0
    var l: Float = 0
    //
    var username: String = ""
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        username <- map[JSONKey.username.rawValue]
        amount <- map[JSONKey.amount.rawValue]
        frequencyId <- map[JSONKey.frequencyId.rawValue]
        period = tranform.transformFromJSON(map[JSONKey.period.rawValue])
        ce <- map[JSONKey.ce.rawValue]
        gs <- map[JSONKey.gs.rawValue]
        re <- map[JSONKey.re.rawValue]
        i <- map[JSONKey.i.rawValue]
        pu <- map[JSONKey.pu.rawValue]
        l <- map[JSONKey.l.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        let json = [JSONKey.username.rawValue:username,
                    JSONKey.amount.rawValue:amount,
                    JSONKey.frequencyId.rawValue:frequencyId,
                    JSONKey.ce.rawValue:ce,
                    JSONKey.gs.rawValue:gs,
                    JSONKey.re.rawValue:re,
                    JSONKey.i.rawValue:i,
                    JSONKey.pu.rawValue:pu,
                    JSONKey.l.rawValue:l
            ] as [String : Any]
        return json
    }
    var rate: Float = 0.02578
    func calculate(n:Float) {
        amount = caculatorZ(n: n, rate: rate, ce: ce, gs: gs, re: re, i: i, pu: pu, l: l)
    }
    
    func caculatorZ(n:Float, rate: Float, ce:Float, gs:Float, re:Float, i:Float, pu:Float, l:Float) -> Float {
        let w = ce + gs + re + i
        let d = pu + l
        let nw = w - d
        if nw < n {
            return 0
        } else {
            return ceil(nw*rate)
        }
    }
}

extension Zakat {
    static var saveKey: String {
        return "Zakat-Save"
    }
    func save() {
        UserDefaults.standard.set(self.toJSON(), forKey: Zakat.saveKey)
    }
    class func loadIfSaved() -> Zakat? {
        guard let savedJson = UserDefaults.standard.object(forKey: saveKey) as? [String : Any] else {
            return nil
        }
        if let object = Mapper<Zakat>().map(JSON: savedJson) {
            return object
        }
        return nil
    }
    
    func copy() -> Zakat {
        let json = self.toJSON()
        if let object = Mapper<Zakat>().map(JSON: json) {
            return object
        }
        return Zakat()
    }
}
