//
//  NewFeed.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/16/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

extension Array {
    func newFeedAttributes() -> NSAttributedString {
        guard let newfeeds = self as? [Feed] else {
            return NSAttributedString(string: "")
        }
        if let first = newfeeds.first {
            let att = NSMutableAttributedString(attributedString: String.newFeedIcon())
            att.append(NSAttributedString(string: " "))
            att.append(NSMutableAttributedString(string: first.description))
            return att
        }
        return NSAttributedString(string: "")
    }
}
struct Feed: Mappable {
    enum JSONKey: String {
        case activityType
        case activityBy
    }
    var activityType: String = ""
    var activityBy: String = ""
    var description: String {
        return activityBy + " " + activityType
    }
    
    init(activityType: String, activityBy: String) {
        self.activityType = activityType
        self.activityBy = activityBy
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        activityType <- map[JSONKey.activityType.rawValue]
        activityBy <- map[JSONKey.activityBy.rawValue]
    }
}
