//
//  Invitation.swift
//  Zakatify
//
//  Created by MinhTran on 11/13/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class Invitation: Mappable {
    enum State: String {
        case other
        case open = "OPEN"
        case accepted = "ACCEPTED"
        case rejected = "REJECTED"
    }
    
    enum JSONKey: String {
        case id = "id"
        case invitee = "invitee"
        case invitor = "invitor"
        case state = "state"
        case date = "dateInvited"
    }
    var id: String = ""
    var invitee: String = ""
    var invitor: String = ""
    var state: State = .other
    var date: Date?
    var tranform: DateFormatterTransform {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let tranform = DateFormatterTransform(dateFormatter: dateFormatter)
        return tranform
    }
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        invitee <- map[JSONKey.invitee.rawValue]
        invitor <- map[JSONKey.invitor.rawValue]
        state <- map[JSONKey.state.rawValue]
        date = tranform.transformFromJSON(map[JSONKey.date.rawValue])
    }
}
