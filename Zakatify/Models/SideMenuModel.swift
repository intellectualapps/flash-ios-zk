//
//  SideMenuModel.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class Count: Mappable {
    enum JSONKey: String {
        case count
    }
    var count: Int = 0
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count <- map[JSONKey.count.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.count.rawValue:count]
    }
}

class ButtonAction: NSObject, Mappable {
    var tag = ""
    var text = ""

    override init() {
        super.init()
    }
    
    convenience init(tag: String, text: String) {
        self.init()
        self.tag = tag
        self.text = text
    }

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        tag <- map["tag"]
        text <- map["text"]
    }

    func toJSON() -> [String : Any] {
        return ["tag":tag, "text": text]
    }
}

class SideMenuModel: Mappable {
    enum JSONKey: String {
        case user = "userDetails"
        case donationProgress = "donationProgess"
        case userBoard
        case notification
        case message
        case counts
        case actionButton = "actionButton"
    }
    var user:UserInfo = UserInfo()
    var userStatus = UserStatus.new()
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user <- map[JSONKey.user.rawValue]
        userStatus.donationProgress <- map[JSONKey.donationProgress.rawValue]
        userStatus.userBoard <- map[JSONKey.userBoard.rawValue]
        userStatus.notification <- map[JSONKey.notification.rawValue]
        userStatus.message <- map[JSONKey.message.rawValue]
        userStatus.userCount <- map[JSONKey.counts.rawValue]
        userStatus.button <- map[JSONKey.actionButton.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.user.rawValue:user.toJSON(),
                JSONKey.donationProgress.rawValue:userStatus.donationProgress.toJSON(),
                JSONKey.userBoard.rawValue:userStatus.userBoard.toJSON(),
                JSONKey.notification.rawValue:userStatus.notification.toJSON(),
                JSONKey.message.rawValue:userStatus.message.toJSON(),
                JSONKey.counts.rawValue: userStatus.userCount.toJSON(),
                JSONKey.actionButton.rawValue: userStatus.button.toJSON()]
    }
}

extension SideMenuModel {
    static let saveKey: String = "Saved-SideMenuModel"
    func save() {
        UserDefaults.standard.set(self.toJSON(), forKey: SideMenuModel.saveKey)
    }
    
    class func load() -> SideMenuModel? {
        guard let savedJson = UserDefaults.standard.object(forKey: saveKey) as? [String : Any] else {
            return nil
        }
        if let object = Mapper<SideMenuModel>().map(JSON: savedJson) {
            return object
        }
        return nil
    }
}
