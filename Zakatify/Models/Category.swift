//
//  Category.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable, Tag {
    
    enum JSONKey: String {
        case id
        case description
    }
    
    var id: Int = 0
    var description: String = ""
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        description <- map[JSONKey.description.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        let json = [JSONKey.id.rawValue:id,
                    JSONKey.description.rawValue:description] as [String : Any]
        return json
    }
}
