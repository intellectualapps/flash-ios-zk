//
//  Qoute.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/24/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class Stock: Model {
    var symbol: String?
    var ask: String?
    var averageDailyVolume: String?
    var bid: String?
    var askRealtime: String?
    var bidRealtime: String?
    var bookValue: String?
    var changePercentChange: String?
    var change: String?
    var commission: String?
    var changeRealtime: String?
    var afterHoursChangeRealtime: String?
    var dividendShare: String?
    var castTradeDate: String?
    var tradeDate: String?
    var earningsShare: String?
    var errorIndicationreturnedforsymbolchangedinvalid: String?
    var ePSEstimateCurrentYear: String?
    var ePSEstimateNextYear: String?
    var ePSEstimateNextQuarter: String?
    var daysLow: String?
    var daysHigh: String?
    var yearLow: String?
    var yearHigh: String?
    var holdingsGainPercent: String?
    var annualizedGain: String?
    var holdingsGain: String?
    var holdingsGainPercentRealtime: String?
    var holdingsGainRealtime: String?
    var moreInfo: String?
    var orderBookRealtime: String?
    var marketCapitalization: String?
    var marketCapRealtime: String?
    var eBITDA: String?
    var changeFromYearLow: String?
    var percentChangeFromYearLow: String?
    var lastTradeRealtimeWithTime: String?
    var changePercentRealtime: String?
    var changeFromYearHigh: String?
    var percentChangeFromYearHigh: String?
    var lastTradeWithTime: String?
    var lastTradePriceOnly: String?
    var highLimit: String?
    var lowLimit: String?
    var daysRange: String?
    var daysRangeRealtime: String?
    var fiftydayMovingAverage: String?
    var twoHundreddayMovingAverage: String?
    var changeFromTwoHundreddayMovingAverage: String?
    var percentChangeFromTwoHundreddayMovingAverage: String?
    var changeFromFiftydayMovingAverage: String?
    var percentChangeFromFiftydayMovingAverage: String?
    var name: String?
    var notes: String?
    var open: String?
    var previousClose: String?
    var pricePaid: String?
    var changeinPercent: String?
    var priceSales: String?
    var priceBook: String?
    var exDividendDate: String?
    var pERatio: String?
    var dividendPayDate: String?
    var pERatioRealtime: String?
    var pEGRatio: String?
    var priceEPSEstimateCurrentYear: String?
    var priceEPSEstimateNextYear: String?
    var sharesOwned: String?
    var shortRatio: String?
    var lastTradeTime: String?
    var tickerTrend: String?
    var oneyrTargetPrice: String?
    var volume: String?
    var holdingsValue: String?
    var holdingsValueRealtime: String?
    var yearRange: String?
    var daysValueChange: String?
    var daysValueChangeRealtime: String?
    var stockExchange: String?
    var dividendYield: String?
    var percentChange: String?
    var currency: String?

    override func parseContentFromDict(dict: [String : Any]) {
        self.symbol = dict.stringForKey(key: "Symbol")
        self.ask = dict.stringForKey(key: "Ask")
        self.averageDailyVolume = dict.stringForKey(key: "AverageDailyVolume")
        self.bid  = dict.stringForKey(key: "Bid")
        self.askRealtime = dict.stringForKey(key: "AskRealtime")
        self.bidRealtime = dict.stringForKey(key: "BidRealtime")
        self.bookValue = dict.stringForKey(key: "BookValue")
        self.changePercentChange = dict.stringForKey(key: "ChangePercentChange")
        self.change = dict.stringForKey(key: "Change")
        self.commission = dict.stringForKey(key: "Commission")
        self.changeRealtime = dict.stringForKey(key: "ChangeRealtime")
        self.afterHoursChangeRealtime = dict.stringForKey(key: "AfterHoursChangeRealtime")
        self.dividendShare = dict.stringForKey(key: "DividendShare")
        self.castTradeDate = dict.stringForKey(key: "CastTradeDate")
        self.tradeDate = dict.stringForKey(key: "TradeDate")
        self.earningsShare = dict.stringForKey(key: "EarningsShare")
        self.errorIndicationreturnedforsymbolchangedinvalid = dict.stringForKey(key: "errorIndicationreturnedforsymbolchangedinvalid")
        self.ePSEstimateCurrentYear = dict.stringForKey(key: "EPSEstimateCurrentYear")
        self.ePSEstimateNextYear = dict.stringForKey(key: "EPSEstimateNextYear")
        self.ePSEstimateNextQuarter = dict.stringForKey(key: "EPSEstimateNextQuarter")
        self.daysLow = dict.stringForKey(key: "DaysLow")
        self.daysHigh = dict.stringForKey(key: "DaysHigh")
        self.yearLow  = dict.stringForKey(key: "YearLow")
        self.yearHigh = dict.stringForKey(key: "YearHigh")
        self.holdingsGainPercent = dict.stringForKey(key: "HoldingsGainPercent")
        self.annualizedGain = dict.stringForKey(key: "AnnualizedGain")
        self.holdingsGain = dict.stringForKey(key: "HoldingsGain")
        self.holdingsGainPercentRealtime = dict.stringForKey(key: "HoldingsGainPercentRealtime")
        self.holdingsGainRealtime = dict.stringForKey(key: "HoldingsGainRealtime")
        self.moreInfo = dict.stringForKey(key: "MoreInfo")
        self.orderBookRealtime = dict.stringForKey(key: "OrderBookRealtime")
        self.marketCapitalization = dict.stringForKey(key: "MarketCapitalization")
        self.marketCapRealtime = dict.stringForKey(key: "MarketCapRealtime")
        self.eBITDA = dict.stringForKey(key: "EBITDA")
        self.changeFromYearLow = dict.stringForKey(key: "ChangeFromYearLow")
        self.percentChangeFromYearLow = dict.stringForKey(key: "PercentChangeFromYearLow")
        self.lastTradeRealtimeWithTime = dict.stringForKey(key: "LastTradeRealtimeWithTime")
        self.changePercentRealtime = dict.stringForKey(key: "ChangePercentRealtime")
        self.changeFromYearHigh = dict.stringForKey(key: "ChangeFromYearHigh")
        self.percentChangeFromYearHigh = dict.stringForKey(key: "PercentChangeFromYearHigh")
        self.lastTradeWithTime = dict.stringForKey(key: "LastTradeWithTime")
        self.lastTradePriceOnly = dict.stringForKey(key: "LastTradePriceOnly")
        self.highLimit = dict.stringForKey(key: "HighLimit")
        self.lowLimit = dict.stringForKey(key: "LowLimit")
        self.daysRange = dict.stringForKey(key: "DaysRange")
        self.daysRangeRealtime = dict.stringForKey(key: "DaysRangeRealtime")
        self.fiftydayMovingAverage = dict.stringForKey(key: "FiftydayMovingAverage")
        self.twoHundreddayMovingAverage = dict.stringForKey(key: "TwoHundreddayMovingAverage")
        self.changeFromTwoHundreddayMovingAverage = dict.stringForKey(key: "ChangeFromTwoHundreddayMovingAverage")
        self.percentChangeFromTwoHundreddayMovingAverage = dict.stringForKey(key: "PercentChangeFromTwoHundreddayMovingAverage")
        self.changeFromFiftydayMovingAverage = dict.stringForKey(key: "ChangeFromFiftydayMovingAverage")
        self.percentChangeFromFiftydayMovingAverage = dict.stringForKey(key: "PercentChangeFromFiftydayMovingAverage")
        self.name = dict.stringForKey(key: "Name")
        self.notes = dict.stringForKey(key: "Notes")
        self.open = dict.stringForKey(key: "Open")
        self.previousClose = dict.stringForKey(key: "PreviousClose")
        self.pricePaid = dict.stringForKey(key: "PricePaid")
        self.changeinPercent = dict.stringForKey(key: "ChangeinPercent")
        self.priceSales = dict.stringForKey(key: "PriceSales")
        self.priceBook = dict.stringForKey(key: "PriceBook")
        self.exDividendDate = dict.stringForKey(key: "ExDividendDate")
        self.pERatio = dict.stringForKey(key: "PERatio")
        self.dividendPayDate = dict.stringForKey(key: "DividendPayDate")
        self.pERatioRealtime = dict.stringForKey(key: "PERatioRealtime")
        self.pEGRatio = dict.stringForKey(key: "PEGRatio")
        self.priceEPSEstimateCurrentYear = dict.stringForKey(key: "PriceEPSEstimateCurrentYear")
        self.priceEPSEstimateNextYear = dict.stringForKey(key: "PriceEPSEstimateNextYear")
        self.sharesOwned = dict.stringForKey(key: "SharesOwned")
        self.shortRatio = dict.stringForKey(key: "ShortRatio")
        self.lastTradeTime = dict.stringForKey(key: "LastTradeTime")
        self.tickerTrend = dict.stringForKey(key: "TickerTrend")
        self.oneyrTargetPrice = dict.stringForKey(key: "OneyrTargetPrice")
        self.volume = dict.stringForKey(key: "Volume")
        self.holdingsValue = dict.stringForKey(key: "HoldingsValue")
        self.holdingsValueRealtime = dict.stringForKey(key: "HoldingsValueRealtime")
        self.yearRange = dict.stringForKey(key: "YearRange")
        self.daysValueChange = dict.stringForKey(key: "DaysValueChange")
        self.daysValueChangeRealtime = dict.stringForKey(key: "DaysValueChangeRealtime")
        self.stockExchange = dict.stringForKey(key: "StockExchange")
        self.dividendYield = dict.stringForKey(key: "DividendYield")
        self.percentChange = dict.stringForKey(key: "PercentChange")
        self.currency = dict.stringForKey(key: "Currency")
    }
}
