//
//  QuoteQuery.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/24/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class QuoteQuery: Model {
    var count: Int?
    var created: String?
    var lang: String?
    var results: StockSearchResult?

    override func parseContentFromDict(dict: [String : Any]) {
        self.count = dict.intForKey(key: "count")
        self.created = dict.stringForKey(key: "created")
        self.lang = dict.stringForKey(key: "lang")
        if let arrResults = dict["results"] as? [[String: Any]]{
            for dict in arrResults {
                results = StockSearchResult(dict: dict)
            }
        }
    }
}
