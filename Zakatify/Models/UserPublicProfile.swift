//
//  UserPublicProfile.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserDonationHistoryInfo: Mappable {
    var charityName = ""
    var charityLogoUrl = ""
    var narrative = ""
    var charityId = 0
    
    enum JSONKey: String {
        case charityName
        case charityLogo
        case narrative
        case charityId
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        charityName <- map[JSONKey.charityName.rawValue]
        charityLogoUrl <- map[JSONKey.charityLogo.rawValue]
        narrative <- map[JSONKey.narrative.rawValue]
        charityId <- map[JSONKey.charityId.rawValue]
    }
    
}

struct UserCount: Mappable {
    enum JSONKey: String {
        case donationsCount
        case portfolioCount
        case followersCount
        case userDonationHistoryNarrative
    }
    var donationCount: Int = 0
    var portfolioCount: Int = 0
    var followerCount: Int = 0
    var userDonationHistoryNarrative: [UserDonationHistoryInfo] = []

    init() {

    }

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        donationCount <- map[JSONKey.donationsCount.rawValue]
        portfolioCount <- map[JSONKey.portfolioCount.rawValue]
        followerCount <- map[JSONKey.followersCount.rawValue]
        userDonationHistoryNarrative <- map[JSONKey.userDonationHistoryNarrative.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.donationsCount.rawValue: donationCount,
                JSONKey.portfolioCount.rawValue: portfolioCount,
                JSONKey.followersCount.rawValue: followerCount
        ]
    }
}

class UserPublicProfile: Mappable {
    enum JSONKey: String {
        case user = "user"
        case donationProgress = "donationProgress"
        case donationProgess = "donationProgess"
        case userBoard
        case notification
        case message
        case counts
        case isFollowing
    }

    var user:ZakatifierInfo = ZakatifierInfo()
    var donationProgress: UserDonationProgess = UserDonationProgess()
    var donationProgess: UserDonationProgess = UserDonationProgess()
    var userBoard: UserBoard = UserBoard()
    var notification: Count = Count()
    var message: Count = Count()
    var counts: UserCount = UserCount()
    var isFollowing: Following = Following()
    init() {
    }
    required init?(map: Map) {
        user <- map[JSONKey.user.rawValue]
        donationProgress <- map[JSONKey.donationProgress.rawValue]
        donationProgess <- map[JSONKey.donationProgess.rawValue]
        userBoard <- map[JSONKey.userBoard.rawValue]
        notification <- map[JSONKey.notification.rawValue]
        message <- map[JSONKey.message.rawValue]
        counts <- map["counts"]
        isFollowing <- map["isFollowing"]
    }

    func mapping(map: Map) {
        user <- map[JSONKey.user.rawValue]
        if user.username.isEmpty {
            user <- map["userDetails"]
        }
        donationProgress <- map[JSONKey.donationProgress.rawValue]
        donationProgess <- map[JSONKey.donationProgess.rawValue]
        userBoard <- map[JSONKey.userBoard.rawValue]
        notification <- map[JSONKey.notification.rawValue]
        message <- map[JSONKey.message.rawValue]
        counts <- map[JSONKey.counts.rawValue]
        isFollowing <- map[JSONKey.isFollowing.rawValue]
    }
}

class Following: Mappable {
    var state: Bool = false
    enum JSONKey: String {
        case state
    }

    init() {

    }

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        state <- map[JSONKey.state.rawValue]
    }
}
